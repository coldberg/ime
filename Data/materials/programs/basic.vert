#version 450

uniform layout(location = 0) mat4 			g_transform;
uniform layout(location = 1) sampler2DArray g_texture0;

in layout(location = 0) vec3 in_position;
in layout(location = 1) vec2 in_texcoord;
//----------------------------------------------
in layout(location = 2) vec3 in_offset;
in layout(location = 3) vec3 in_color;
in layout(location = 4) float in_index;

out vec3 v2f_color;
out vec2 v2f_texcoord;
out vec4 v2f_position;
out float v2f_index;

void main () {	
	v2f_color = in_color;
	v2f_texcoord = in_texcoord;
	v2f_position = g_transform * vec4 (in_position + in_offset, 1);
	v2f_index = in_index;
	gl_Position = v2f_position;
}

