%info
program
{
	name "basic"
	properties 
	{			
		depth
		{			
			write			false
			test			false
;			func   			less
		}			
		blend				
		{
			enabled			true
			src_factor		src_alpha
			dst_factor		one_minus_src_alpha
			equation		add
		}
	}		
	pass
	{
		vert "materials/programs/basic.vert"
		frag "materials/programs/basic.frag"
	}
}