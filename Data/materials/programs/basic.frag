#version 450	

uniform layout(location = 0) mat4 			g_transform;
uniform layout(location = 1) sampler2DArray g_texture0;

in  vec2 			v2f_texcoord;
in  vec3 			v2f_color; 
in  vec4 			v2f_position;
in  float 			v2f_index;
out vec4 			out_color;	


void main () {
	vec4 tcol = texture(g_texture0, vec3 (v2f_texcoord, v2f_index));
	//out_color = vec4 (mix (tcol.xyz, tcol.xyz*v2f_color, smoothstep (100.0f, 500.0f, v2f_position.z)), tcol.w);
	out_color = mix (tcol * vec4 (v2f_color, 1.0f), tcol, 
					 1.0);
					 //1.0f - smoothstep (75.0f, 250.0f, v2f_position.z));
}