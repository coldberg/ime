#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "ime_asset_base.hpp"
#include "ime_asset_locator.hpp"

namespace ime {
    
    struct asset_cache_interface {
        
        virtual asset_shared fetch (const std::string& s_path, const std::string& s_override_type = "") = 0;
        virtual bool map_asset (const std::string& s_path, asset_locator_unique s_locator) = 0;
        virtual bool unmap_asset (const std::string& s_path) = 0;
        virtual ~asset_cache_interface () = default;

        template <typename _Locator, typename... _Args> 
        auto map_asset_as_ (const std::string& s_path, _Args&&... args) {
            return map_asset (s_path, _Locator::new_unique (std::forward<_Args> (args)...));
        }

        template <typename _Asset, typename... _Args>
        auto fetch_as_ (_Args&&... args) {
            auto s_asset = fetch (std::forward<_Args> (args)...);
            if (s_asset == nullptr) {
                ime::log.warn (__FILE__ ": unable to fetch asset.");
                return std::shared_ptr<const _Asset> ();
            }
            return s_asset->as_<_Asset> ();                
        }
    };


    asset_cache_interface& asset_cache ();
    

};
