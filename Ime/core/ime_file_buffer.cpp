#include "ime_file_buffer.hpp"
#include <algorithm>

ime::immutable_file_buffer::~immutable_file_buffer () {}

std::size_t ime::immutable_file_buffer::size () const {    
    return m_size;
}

std::uint64_t ime::immutable_file_buffer::seek (std::uint64_t s_offset) {    
    if (is_good () != true) {
        return tell ();
    }

    if (m_fptr >= m_size) {
        last_error (io_status_end_of_file);
        return tell ();
    }

    const auto s_fptr = m_fptr;
    m_fptr = s_offset;
    return s_fptr;
}

std::uint64_t ime::immutable_file_buffer::tell () const {
    return m_fptr;
}

std::size_t ime::immutable_file_buffer::read (void* s_buff, std::size_t s_length) {
    if (is_good () != true) {
        return 0u;
    }
    if (m_fptr >= m_size) {
        last_error (io_status_end_of_file);
        return 0u;
    }
    auto s_to_read = std::min<std::size_t> (s_length, m_size - m_fptr);
    std::memcpy (s_buff, m_buff + m_fptr, s_to_read);
    m_fptr += s_to_read;
    return s_to_read;
}

const void* ime::immutable_file_buffer::map_read_only (std::uint64_t s_offset, std::size_t s_length) {
    if (is_good () != true) {        
        return nullptr;
    }
    if (s_offset >= m_size) {
        last_error (io_status_end_of_file);
        return nullptr;
    }

    if (s_offset + s_length > m_size) {        
        //auto s_to_read = std::min<std::uint64_t> (s_length, m_size - s_offset);
        last_error (io_status_end_of_file);
        return nullptr;
    }
    return m_buff + s_offset;
}

void ime::immutable_file_buffer::unmap () {

}

ime::immutable_file_buffer::immutable_file_buffer (const void* s_buff, std::size_t s_size):
    m_buff ((const std::uint8_t*)s_buff),
    m_size (s_size),
    m_fptr (0u)
{
    if (m_buff == nullptr || m_size == 0u) {
        last_error (io_status_file_not_open);
    }
    else {
        last_error_clear ();
    }
}

void ime::immutable_file_buffer::set_underlying_data_buffer (const void* s_buff, std::size_t s_size) {    
    m_buff = (const std::uint8_t *)s_buff;
    m_size = s_size;

    if (m_buff == nullptr || m_size == 0u) {
        last_error (io_status_file_not_open);        
    }
    else {
        last_error_clear ();
    }
}
const void* ime::immutable_file_buffer::underlying_data_buffer () const {
    return m_buff;
}

