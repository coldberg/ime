#pragma once

#include "ime_asset_locator.hpp"

namespace ime {

    struct asset_locator_data:
        public asset_locator_t<asset_locator_data>
    {
        enum flags_type: std::uint32_t {
            copy_data = 1u
        };

        asset_locator_data (const std::string& s_fake_path,
                            const std::string& s_mime_type,
                            const void* s_data_addr, 
                            std::size_t s_data_size, 
                            std::uint32_t s_flags = copy_data);

        ~asset_locator_data () override;

        const char* type () const override;
        const char* location () const override;
        auto access () const -> std::shared_ptr<ime::immutable_file_base> override;

        virtual const void* data () const;
        virtual std::size_t size () const;

    private:
        static const void* duplicate_data (const void* s_data_addr, std::size_t s_data_size);
        std::string   m_mime_type;
        std::size_t   m_data_size;
        const void*   m_data_addr;
        std::uint32_t m_file_flag;
    };

}
