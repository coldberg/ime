#include "ime_asset_cache.hpp"
#include "ime_asset_locator_data.hpp"
#include "ime_utils.hpp"

//struct text_asset_def {
//    const char* s_path;
//    const char* s_type;
//    const char* s_text;
//    std::size_t s_size;
//};
//
//static const text_asset_def g_builtin_resources [] = {
//};

//IME_BEFORE_MAIN ([] () {
//    for (const auto& s_item : g_builtin_resources) {
//        ime::asset_cache ().map_asset (
//            s_item.s_path, 
//            ime::asset_locator_data::new_unique (
//                s_item.s_path,
//                s_item.s_type,
//                s_item.s_text, 
//                s_item.s_size, 
//                0u
//            )
//        );
//    }
//});