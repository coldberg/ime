#pragma once

#include "ime_asset_base.hpp"
#include "ime_asset_loader.hpp"

namespace ime {

    struct asset_loader_text:
        public asset_loader 
    {
        asset_shared load (const std::string& s_key, const asset_locator&) const override;

        asset_loader_text ();
    };

}
