#include "ime_log.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_asset_loader_image_tga.hpp"

#include <cstdint>
#include <cstddef>
#include <memory>

enum image_data_type: std::uint8_t {
    no_image_data               = 0,
    uncompressed_indexed        = 1,
    uncompressed_rgb            = 2,
    uncompressed_mono           = 3,
    compressed_rle_indexed      = 9,
    compressed_rle_rgb          = 10,
    compressed_rle_mono         = 11,
    compressed_huff1_indexed    = 32,
    compressed_huff2_indexed    = 33     
};

#pragma pack(push, 1)
struct tga_header {
    std::uint8_t    id_length;
    std::uint8_t    color_map_type;
    std::uint8_t    data_type_code;
    std::uint16_t   color_map_origin;
    std::uint16_t   color_map_length;
    std::uint8_t    color_map_depth;
    std::int16_t    x_origin;
    std::int16_t    y_origin;
    std::uint16_t   width;
    std::uint16_t   height;
    std::uint8_t    bits_per_pixel;
    std::uint8_t    image_descriptor;
    std::uint8_t    data [1];
};
#pragma pack(pop)

ime::asset_shared ime::asset_loader_image_tga::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_file = s_locator.access ();
    if (!s_file || !s_file->is_good ()) {
        ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
        return nullptr;
    }

    auto s_size = s_file->size ();
    auto s_header = s_file->map_read_only_as_<tga_header> (0u, s_size);

    if (!s_file->is_good ()) {
        ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
        return nullptr;
    }
    
    if (s_header->data_type_code != uncompressed_rgb) {
        ime::log.warn (__FILE__ ": only uncompressed rgb supported.");
        return nullptr;
    }

    const image_format_descriptor* s_format = nullptr;
    auto s_fix_alpha = false;
    switch (s_header->bits_per_pixel) {
        case 32:
        {
            switch (s_header->image_descriptor & 0xf) {
                case 0: s_fix_alpha = true;
                case 8: s_format = &ime::image_format_argb8888; break;
                default:
                {
                    ime::log.warn (__FILE__ ": 32-bit images alpha size must either be 0 or 8-bits.");
                    return nullptr;
                }
            }                
            break;
        }
        case 24:
        {
            if (s_header->image_descriptor & 0xf) {
                ime::log.warn (__FILE__ ": 24-bit images with alpha not supported.");
                return nullptr;
            }
            s_format = &ime::image_format_rgb888; 
            break;
        }
        case 16:
        {       
            switch (s_header->image_descriptor & 0xf) {
                case 0 : s_format = &ime::image_format_rgb555; break;
                case 1 : s_format = &ime::image_format_argb1555; break;
                default:
                {
                    ime::log.warn (__FILE__ ": 16bit images with more then 1-bit alpha are not supported.");
                    return nullptr;
                }
            }                
            break;
        }
        default:
        {
            ime::log.warn (__FILE__ ": only 16, 24 and 32 bit images are supported.");
            return nullptr;             
        }
    }

    const auto s_pixel_size = s_format->size_in_bytes ();
    const auto s_row_stride = s_pixel_size * s_header->width;
    const auto s_image_size = s_row_stride * s_header->height ;

    if (s_image_size + sizeof (tga_header) > s_file->size ()) {
        ime::log.warn (__FILE__ ": image seems corrupt.");
        return nullptr;
    }

    const auto s_flip_x = (s_header->image_descriptor & (1u << 4)) ? true : false; 
    const auto s_flip_y = (s_header->image_descriptor & (1u << 5)) ? false : true;

    auto s_image_data = std::make_unique<std::uint8_t []> (s_image_size);

    const auto* s_source_ptr = &s_header->data [0]; 

    for (auto j = 0; j < s_header->height; ++j) 
    {
        for (auto i = 0; i < s_header->width; ++i) 
        {
            const auto y = s_flip_y ? s_header->height - j - 1 : j;
            const auto x = s_flip_x ? s_header->width  - i - 1 : i;

            std::memcpy (&s_image_data [j * s_row_stride + i * s_pixel_size],
                         &s_source_ptr [y * s_row_stride + x * s_pixel_size],
                         s_pixel_size);

            if (s_fix_alpha) {
                auto& s_pixel = reinterpret_cast<std::uint32_t&> (
                    s_image_data [j * s_row_stride + i * s_pixel_size]);
                s_pixel |= s_format->alpha_mask ();
            }
        }
    }
  

    return asset_image::new_shared (std::move (s_image_data), s_header->width, s_header->height, *s_format);
    
    
}

ime::asset_loader_image_tga::asset_loader_image_tga () {
    ime::asset_loader::register_loader ("image/tga",
    [this] (const std::string& s_mime_type)
        -> ime::asset_loader& 
    {
        return *this;
    });
}

static ime::asset_loader_image_tga g_asset_loader_tga;
