#pragma once

#include <cstddef>
#include <cstdint>

namespace ime {
    enum io_status_type {
        io_status_success,
        io_status_end_of_file,
        io_status_file_not_open
    };

    struct immutable_file_base {
        enum last_error_clear: bool {
            get_dont_clear = false,
            get_and_clear = true             
        };

        immutable_file_base ();
        virtual ~immutable_file_base () = default;

        virtual std::size_t size () const = 0;

        virtual std::uint64_t seek (std::uint64_t s_offset) = 0;
        virtual std::uint64_t tell () const = 0;

        virtual std::size_t read (void* s_buff, std::size_t s_length) = 0;

        virtual const void* map_read_only (std::uint64_t s_offset, std::size_t s_size) = 0;
        virtual void unmap () = 0;

        virtual void last_error (io_status_type s_error);
        virtual io_status_type last_error () const;

        io_status_type last_error_clear ();
        bool is_good () const;

        template <typename T>
        const T* map_read_only_as_ (std::uint64_t s_offset, std::size_t s_size) {
            return reinterpret_cast<const T*> (map_read_only (s_offset, s_size));
        }

    private:
        io_status_type m_last_error;
    };

    struct mutable_file_base: 
        public immutable_file_base 
    {
        virtual std::size_t write (const void* s_buff, std::size_t s_length) = 0;
        virtual void* map_read_write (std::uint64_t s_offset, std::size_t s_size, bool s_private) = 0;

        template <typename T>
        T* map_read_write_as_ (std::uint64_t s_offset, std::size_t s_size, bool s_private) {
            return reinterpret_cast<T*> (map_read_write (s_offset, s_size, s_private));
        }

    };

}
