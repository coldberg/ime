#pragma once

#include "ime_asset_base.hpp"
#include "ime_asset_loader.hpp"

namespace ime {
    
    struct asset_loader_ptree:
        public asset_loader 
    {
        asset_shared load (const std::string& s_key, const asset_locator&) const override;
        asset_loader_ptree ();


    private:
        struct json: public asset_loader { asset_shared load (const std::string& s_key, const asset_locator&) const override; };
        struct info: public asset_loader { asset_shared load (const std::string& s_key, const asset_locator&) const override; };
        struct  xml: public asset_loader { asset_shared load (const std::string& s_key, const asset_locator&) const override; };
        struct  ini: public asset_loader { asset_shared load (const std::string& s_key, const asset_locator&) const override; };

        json m_json_loader;
        xml  m_xml_loader;
        info m_info_loader;
        ini  m_ini_loader;

    };    

}

