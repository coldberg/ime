#pragma once 

#include <memory>

namespace ime {
    struct gl_asset_texture;

    typedef std::shared_ptr<const gl_asset_texture> gl_asset_texture_shared;
    typedef std::unique_ptr<const gl_asset_texture> gl_asset_texture_unique;
    typedef std::weak_ptr  <const gl_asset_texture> gl_asset_texture_weak;
}
