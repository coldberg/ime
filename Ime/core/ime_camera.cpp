#include "ime_camera.hpp"
#include "ime_gl_context.hpp"

const ime::mat4& ime::camera::view () const {    
    return m_view;
}

const ime::mat4& ime::camera::projection () const {
    return m_projection;
}

ime::mat4 ime::camera::projection_view () const {
    return projection () * view ();
}

const std::pair<ime::vec2, ime::vec2>& ime::camera::viewport () const {
    return m_viewport;
}

const std::pair<ime::vec2, ime::vec2>& ime::camera::scissor () const {
    return m_scissor;
}

void ime::camera::viewport (const std::pair<ime::vec2, ime::vec2>& s_value) {
    m_viewport = s_value;
}

void ime::camera::scissor (const std::pair<ime::vec2, ime::vec2>& s_value) {
    m_scissor = s_value;
}

void ime::camera::cache_view (const ime::mat4& s_value) const {
    m_view = s_value;
}

void ime::camera::cache_projection (const ime::mat4& s_value) const {
    m_projection = s_value;
}



