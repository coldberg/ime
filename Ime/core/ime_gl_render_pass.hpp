#pragma once

#include "ime_gl_context_fwd.hpp"
#include "ime_gl_asset_texture_fwd.hpp"
#include "ime_gl_asset_shader_fwd.hpp"
#include "ime_gl_handle_fwd.hpp"
#include "ime_gl_enum.hpp"

#include <vector>

namespace ime {

    struct gl_render_pass {

        struct texture_reference_type {
            texture_reference_type (                
                const gl_texture_handle& s_texture,
                gl_texture_target_enum_type s_target = gl_enum_texture2d)
            :   target (s_target),
                texture (s_texture) 
            {}

            gl_texture_target_enum_type target;
            const gl_texture_handle& texture;
        };

        struct draw_call_type {
            draw_call_type (const gl_vertex_array_handle& s_vertex_array,
                gl_draw_mode_enum_type s_mode = gl_enum_triangles,
                std::uint32_t s_length = 0u,   
                std::uint32_t s_instances = 0u,
                std::uint32_t s_offset = 0u)
            :   vertex_array (s_vertex_array),
                mode (s_mode),
                length (s_length),
                offset (s_offset),
                instances (s_instances)
            {}

            const gl_vertex_array_handle& vertex_array;
            gl_draw_mode_enum_type mode = gl_enum_triangles;
            std::uint32_t length = 0u;
            std::uint32_t offset = 0u;
            std::uint32_t instances = 0u;
        };

        gl_asset_shader_weak program;

        std::vector<texture_reference_type> textures;        
        std::vector<draw_call_type> draw_calls;

        template<typename... Args>
        void add_draw_call (Args&&... args) {
            draw_calls.emplace_back (std::forward<Args> (args)...);
        }

        template<typename... Args>
        void add_texture (Args&&... args) {
            textures.emplace_back (std::forward<Args> (args)...);
        }

        void clear_draw_calls () {
            draw_calls.clear ();
        }

        void clear_textures () {
            textures.clear ();
        }
    };


}
