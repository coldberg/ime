#include "ime_asset_type_blob.hpp"

ime::asset_blob::asset_blob (std::unique_ptr<std::uint8_t []> s_data, std::size_t s_size):
    m_data (std::move (s_data)),
    m_size (s_size) 
{}

const void* ime::asset_blob::data () const { return m_data.get (); }
std::size_t ime::asset_blob::size () const { return m_size; }
