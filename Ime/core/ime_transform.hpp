#pragma once

#include "ime_math.hpp"

namespace ime {
    struct transform {
        ime::mat4 m_projection;
        ime::mat4 m_view;
        ime::mat4 m_projection_view;
    };
}
