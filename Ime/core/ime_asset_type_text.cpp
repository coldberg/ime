#include "ime_asset_type_text.hpp"

const char* ime::asset_text::cstring () const {
    return m_text.get ();
}

std::string ime::asset_text::string () const {
    return std::string (cstring (), length ());
}

std::size_t ime::asset_text::length () const {
    return m_size;
}

ime::asset_text::asset_text (std::unique_ptr<const char []> m_text, std::size_t m_size):
    m_text (std::move (m_text)),
    m_size (m_size)
{
}

inline const void * ime::asset_text::data () const { return cstring (); }

inline std::size_t ime::asset_text::size () const { return length (); }
