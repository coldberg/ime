#include "ime_asset_loader_image_jpg.hpp"
#include "ime_image_format.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_log.hpp"
#include <memory>
#include <jpeglib.h>

ime::asset_shared ime::asset_loader_image_jpg::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_file = s_locator.access ();
    if (!s_file || !s_file->is_good ()) {
        ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
        return nullptr;
    }

    auto s_size = s_file->size ();
    auto s_data = s_file->map_read_only (0u, s_size);

    if (!s_file->is_good ()) {
        ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
        return nullptr;
    }

    jpeg_decompress_struct s_decompress;
    jpeg_error_mgr s_error;

    s_decompress.err = jpeg_std_error (&s_error);
    jpeg_create_decompress (&s_decompress);
    jpeg_mem_src (&s_decompress, (const std::uint8_t *)s_data, (std::uint32_t)s_size);

    if (jpeg_read_header (&s_decompress, TRUE) != 1) {
        ime::log.warn (__FILE__ ": unable to read header.");
        return nullptr;
    }

    jpeg_start_decompress (&s_decompress);
    //s_decompress.image_width * s_decompress.image_height;
    const ime::image_format_descriptor* s_format = nullptr;
    switch (s_decompress.out_color_space) {
        case JCS_RGB:
        case JCS_GRAYSCALE:
        {
            switch (s_decompress.out_color_components) {
                case 1: s_format = &ime::image_format_g8; break;
                case 3: s_format = &ime::image_format_bgr888; break;
                default:
                {
                    ime::log.warn (__FILE__ ": only grayscale and rgb images supported.");
                    return nullptr;
                }
            }
            break;
        }
        default:
        {
            ime::log.warn (__FILE__ ": only grayscale and rgb images supported.");
            return nullptr;
        }
    }

    auto s_pixel_size = s_format->size_in_bytes ();
    auto s_row_stride = s_pixel_size * s_decompress.output_width;
    auto s_total_size = s_row_stride * s_decompress.output_height;
    auto s_image_data = std::make_unique<std::uint8_t []> (s_total_size);

    for (auto j = 0u; j < s_decompress.output_height; ++j) {
        auto s_ptr = &s_image_data [j * s_row_stride];
        if (jpeg_read_scanlines (&s_decompress, (JSAMPARRAY)&s_ptr, 1) != 1) {
            ime::log.warn (__FILE__ ": wasn't able to read entire image.");
            return nullptr;
        }
    }

    return asset_image::new_shared (
        std::move (s_image_data), 
        s_decompress.output_width, 
        s_decompress.output_height, 
        *s_format
    );
}



ime::asset_loader_image_jpg::asset_loader_image_jpg () {
    ime::asset_loader::register_loader ("image/jpg",
    [this] (const std::string& s_mime_type)
        -> ime::asset_loader& 
    {
        return *this;
    });
}

static ime::asset_loader_image_jpg g_asset_loader_jpg;
