#pragma once

#include <memory>

#include "ime_asset_base.hpp"
#include "ime_gl_context.hpp"
#include "ime_gl_shader_pass.hpp"

namespace ime {

    struct gl_asset_shader final:
        public asset_base_t<gl_asset_shader>
    {
        gl_asset_shader (std::string s_name, std::vector<gl_shader_pass>, gl_shader_props s_params = gl_shader_props::default_props ());
       ~gl_asset_shader () override;
        
        const std::vector<gl_shader_pass>& passes () const;
        const std::string& name () const;
        const gl_shader_props& props () const;
    private:  
        std::vector<gl_shader_pass> m_passes;
        gl_shader_props m_params;
        std::string m_name;
    };

}
