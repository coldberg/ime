#include "ime_asset_loader_ptree.hpp"
#include "ime_asset_type_ptree.hpp"
#include "ime_asset_type_text.hpp"

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <sstream>
#include <unordered_map>

static ime::asset_ptree::ptree_type detect_ptree_type (std::stringstream& s_stream, const std::string& s_string) {
    const std::unordered_map<std::string, ime::asset_ptree::ptree_type> g_tree_type_map = {
        {"%json", ime::asset_ptree::ptree_type_json},
        {"%info", ime::asset_ptree::ptree_type_info},
        {"%xml",  ime::asset_ptree::ptree_type_xml},
        {"%ini",  ime::asset_ptree::ptree_type_ini}
    };

    s_stream.str (s_string);
    if (s_string [0] == '%') {
        std::string s_header;        
        std::getline (s_stream, s_header);
        auto it = g_tree_type_map.find (s_header);
        if (it != g_tree_type_map.end ()) {
            return it->second;
        }
        return ime::asset_ptree::ptree_type_unknown;
    }

    auto i = std::distance (s_string.begin (), std::find_if (s_string.begin (), s_string.end (), [] (auto c) { return c > 32; }));

    if (s_string.find ("<?xml", i) == i) {
        return ime::asset_ptree::ptree_type_xml;
    }
    
    return ime::asset_ptree::ptree_type_unknown;
}

ime::asset_shared ime::asset_loader_ptree::load (const std::string& s_key, const asset_locator& s_locator) const {
    try {                
        auto s_text = ime::plain_load_as_<ime::asset_text> (s_key, s_locator, "text/plain");
        if (s_text == nullptr) {
            ime::log.warn (__FILE__ ": unable to load %s.", s_locator.location ());
            return nullptr;
        }

        boost::property_tree::ptree s_tree;
        std::stringstream s_stream;
        auto s_ptree_type = detect_ptree_type (s_stream, s_text->string ());

        static const auto trim_whitespace = boost::property_tree::xml_parser::trim_whitespace;

        switch (s_ptree_type) {
            case ime::asset_ptree::ptree_type_info : boost::property_tree::read_info (s_stream, s_tree); break;
            case ime::asset_ptree::ptree_type_json : boost::property_tree::read_json (s_stream, s_tree); break;
            case ime::asset_ptree::ptree_type_ini  : boost::property_tree::read_ini  (s_stream, s_tree); break;
            case ime::asset_ptree::ptree_type_xml  : boost::property_tree::read_xml  (s_stream, s_tree, trim_whitespace); break;
            
            default:
            {
                log.warn (__FILE__ ": unable to determine format for %s.", s_locator.location ());

                
                static const std::vector<ime::asset_ptree::ptree_type> s_attempt = 
                {
                    ime::asset_ptree::ptree_type_info,
                    ime::asset_ptree::ptree_type_json,
                    ime::asset_ptree::ptree_type_ini,
                    ime::asset_ptree::ptree_type_xml
                };

                for (const auto& s_type : s_attempt)  
                {
                    try {                        
                        s_tree.clear ();
                        s_stream.str (s_text->string ());
                        switch (s_type) {
                            case ime::asset_ptree::ptree_type_info : log.warn (__FILE__ ": attempting to parse as info."); boost::property_tree::read_info (s_stream, s_tree); break;
                            case ime::asset_ptree::ptree_type_json : log.warn (__FILE__ ": attempting to parse as json."); boost::property_tree::read_json (s_stream, s_tree); break;
                            case ime::asset_ptree::ptree_type_ini  : log.warn (__FILE__ ": attempting to parse as ini." ); boost::property_tree::read_ini  (s_stream, s_tree); break;
                            case ime::asset_ptree::ptree_type_xml  : log.warn (__FILE__ ": attempting to parse as xml." ); boost::property_tree::read_xml  (s_stream, s_tree, trim_whitespace); break;
                        }
                        return ime::asset_ptree::new_shared (std::move (s_tree), ime::asset_ptree::ptree_type_json);
                    }
                    catch (...) {}
                }

                log.warn (__FILE__ ": failed to parse ptree.");
                return nullptr;
            }

        }
        
        return ime::asset_ptree::new_shared (std::move (s_tree), s_ptree_type);
    }
    catch (const std::exception& s_ex) {
        log.warn (__FILE__ ": unable to parse %s: %s.", s_locator.type (), s_ex.what ());
    }
    return nullptr;
}

ime::asset_loader_ptree::asset_loader_ptree () {
    typedef std::unordered_map<std::string, ime::asset_loader*> mapping_type;
    auto p_loader_map = std::make_shared<mapping_type> (mapping_type {
        {"text/json",  &m_json_loader},
        {"text/info",  &m_info_loader},
        {"text/xml",   &m_xml_loader},
        {"text/ini",   &m_ini_loader},
        {"text/ptree", this}
    });

    auto g_loader_function = [p_loader_map] (const std::string& s_mime_type) noexcept -> ime::asset_loader& 
    {
        return *(*p_loader_map).at (s_mime_type);
    };

    ime::asset_loader::register_loader ("text/ptree", g_loader_function);
    ime::asset_loader::register_loader ("text/json", g_loader_function);
    ime::asset_loader::register_loader ("text/info", g_loader_function);
    ime::asset_loader::register_loader ("text/xml", g_loader_function);
    ime::asset_loader::register_loader ("text/ini", g_loader_function);
}


static ime::asset_loader_ptree g_ptree_loader;

ime::asset_shared ime::asset_loader_ptree::json::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_text = ime::plain_load_as_<ime::asset_text> (s_key, s_locator, "text/plain");
    if (s_text == nullptr) {
        ime::log.warn (__FILE__ ": unable to load %s.", s_locator.location ());
        return nullptr;
    }
    boost::property_tree::ptree s_tree;
    std::stringstream s_stream (s_text->cstring ());
    boost::property_tree::read_json (s_stream, s_tree);
    return ime::asset_ptree::new_shared (std::move (s_tree), ime::asset_ptree::ptree_type_json);
}

ime::asset_shared ime::asset_loader_ptree::info::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_text = ime::plain_load_as_<ime::asset_text> (s_key, s_locator, "text/plain");
    if (s_text == nullptr) {
        ime::log.warn (__FILE__ ": unable to load %s.", s_locator.location ());
        return nullptr;
    }
    boost::property_tree::ptree s_tree;
    std::stringstream s_stream (s_text->cstring ());
    boost::property_tree::read_info (s_stream, s_tree);
    return ime::asset_ptree::new_shared (std::move (s_tree), ime::asset_ptree::ptree_type_info);
}

ime::asset_shared ime::asset_loader_ptree::xml::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_text = ime::plain_load_as_<ime::asset_text> (s_key, s_locator, "text/plain");
    if (s_text == nullptr) {
        ime::log.warn (__FILE__ ": unable to load %s.", s_locator.location ());
        return nullptr;
    }
    boost::property_tree::ptree s_tree;
    std::stringstream s_stream (s_text->cstring ());
    boost::property_tree::read_xml (s_stream, s_tree, boost::property_tree::xml_parser::trim_whitespace);
    return ime::asset_ptree::new_shared (std::move (s_tree), ime::asset_ptree::ptree_type_xml);
}

ime::asset_shared ime::asset_loader_ptree::ini::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_text = ime::plain_load_as_<ime::asset_text> (s_key, s_locator, "text/plain");
    if (s_text == nullptr) {
        ime::log.warn (__FILE__ ": unable to load %s.", s_locator.location ());
        return nullptr;
    }
    boost::property_tree::ptree s_tree;
    std::stringstream s_stream (s_text->cstring ());
    boost::property_tree::read_ini (s_stream, s_tree);
    return ime::asset_ptree::new_shared (std::move (s_tree), ime::asset_ptree::ptree_type_ini);
}
