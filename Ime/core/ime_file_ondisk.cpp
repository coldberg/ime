#include "ime_file_ondisk.hpp"

ime::immutable_file_ondisk::immutable_file_ondisk (const std::string& s_path):
    m_file (s_path),
    immutable_file_buffer (nullptr, 0u)
{
    if (m_file.is_open ()) {
        set_underlying_data_buffer (m_file.data (), m_file.size ());
    }
}


