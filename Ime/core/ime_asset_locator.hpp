#pragma once

#include <string>
#include <memory>

#include "ime_asset_base.hpp"
#include "ime_file_base.hpp"

namespace ime {
    struct asset_locator {
        virtual ~asset_locator () = default;
        virtual const char* type () const = 0;
        virtual const char* location () const = 0;
        virtual auto access () const -> std::shared_ptr<ime::immutable_file_base>  = 0;
    };

    typedef std::shared_ptr<asset_locator> asset_locator_shared;
    typedef std::unique_ptr<asset_locator> asset_locator_unique;

    template <typename _SubClass>
    struct asset_locator_t: 
        public asset_locator 
    {
        template <typename... Args>
        static auto new_shared (Args&&... args) {
            return std::make_shared<_SubClass> (std::forward<Args> (args)...);
        }
        template <typename... Args>
        static auto new_unique (Args&&... args) {
            return std::make_unique<_SubClass> (std::forward<Args> (args)...);
        }
    };
}