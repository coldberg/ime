#include <SDL.h>
#include <GL/glew.h>

/////////////////////////////////////

#include <iostream>
#include <string>
#include <cassert>

#include "ime_math.hpp"
#include "ime_asset_locator_file.hpp"
#include "ime_asset_cache.hpp"
#include "ime_asset_loader.hpp"
#include "ime_asset_type_text.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_gl_asset_shader.hpp"
#include "ime_gl_asset_texture.hpp"
#include "ime_gl_render_pass.hpp"
#include "ime_gl_context.hpp"
#include "ime_camera_perspective.hpp"
#include "ime_example_render.hpp"

////////////////////////////////////

int main (int, char**) try 
{
    SDL_Init (SDL_INIT_EVERYTHING);
    std::atexit (SDL_Quit);

    auto s_context = ime::gl_context::lock ();

    ime::camera_perspective s_camera (
        {
            {0.0f, 0.0f, -1000.0f}, 
            {0.0f, 0.0f, 1.0f}, 
            {0.0f, 1.0f, 0.0f}
        },
        1280.0f/780.0f,
        45.0f,
        0.1f,
        2000.0f
    );

    ime::example_render m_render (*s_context);

    SDL_SetRelativeMouseMode (SDL_TRUE);

    ime::ivec2 s_mouse_delta (0);

    ime::frustrum s_frustrum;
    ime::timer s_timest;
    ime::transform s_transform;

    while (!SDL_QuitRequested ()) {        
        SDL_Event s_event;
        if (SDL_PollEvent (&s_event)) {
            switch (s_event.type) {
                case SDL_MOUSEMOTION:
                {
                    s_mouse_delta.x += s_event.motion.xrel;
                    s_mouse_delta.y += s_event.motion.yrel;
                    break;
                }
            }
            continue;
        }

        s_timest.advance ();
        {
            const auto* s_keys = SDL_GetKeyboardState (nullptr);
            const auto lt = 1.0f*s_keys [SDL_SCANCODE_A];
            const auto rt = 1.0f*s_keys [SDL_SCANCODE_D];
            const auto up = 1.0f*s_keys [SDL_SCANCODE_SPACE];
            const auto dn = 1.0f*s_keys [SDL_SCANCODE_C];
            const auto fw = 1.0f*s_keys [SDL_SCANCODE_W];
            const auto bw = 1.0f*s_keys [SDL_SCANCODE_S];
            const auto rl = 1.0f*s_keys [SDL_SCANCODE_Q];
            const auto rr = 1.0f*s_keys [SDL_SCANCODE_E];

            s_camera.move (1e3f*s_timest.delta_time ()*(
                 (lt - rt)*s_camera.rightward ()
                +(up - dn)*s_camera.upward ()
                +(fw - bw)*s_camera.forward ()
            ));

            const auto q0 = ime::angleAxis (-5e-4f*s_mouse_delta.x, s_camera.upward ());
            const auto q1 = ime::angleAxis (+5e-4f*s_mouse_delta.y, s_camera.rightward ());
            const auto q2 = ime::angleAxis ((rr - rl)*s_timest.delta_time (), s_camera.forward ());
            s_camera.rotate (q0*q1*q2);

            //s_camera.rotate (q0 * q1 * q2);
            s_mouse_delta = ime::ivec2 (0);
        }

        s_context->clear ({ime::gl_clear_color_buffer_bit, ime::gl_clear_depth_buffer_bit}, ime::vec4 (1.0f, 1.0f, 1.0f, 1.0f), 1.0f);

        s_transform.m_projection = s_camera.projection ();
        s_transform.m_view = s_camera.view ();        
        s_transform.m_projection_view = s_transform.m_projection * s_transform.m_view;
        m_render.render_frame (s_timest, s_transform, s_frustrum);
        s_context->present ();
    }
    return 0;
}
catch (std::exception& ex) {
    std::cout << ex.what () << "\n";
    __debugbreak ();
}
