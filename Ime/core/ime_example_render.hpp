#pragma once 

#include "ime_singleton.hpp"
#include "ime_math.hpp"
#include "ime_gl_context_fwd.hpp"
#include "ime_gl_asset_shader_fwd.hpp"
#include "ime_gl_asset_texture_fwd.hpp"
#include "ime_gl_handle_fwd.hpp"
#include "ime_gl_render_pass.hpp"
#include "ime_camera_perspective.hpp"
#include "ime_frustrum.hpp"
#include "ime_timer.hpp"
#include "ime_transform.hpp"

namespace ime {


    struct example_render: 
        public singleton<example_render> 
    {
        example_render (gl_context&);

        void update_objects (const timer&, const transform&);
        void render_frame (const timer&, const transform&, const frustrum&);

    private:
        static const auto g_number_of_instances = 32u;

        gl_context& m_context;

        gl_asset_texture_shared m_texture;
        gl_asset_shader_shared  m_program;
        gl_vertex_array_handle m_mesh_array;
        gl_buffer_handle m_mesh_buffer_per_vertex;
        gl_buffer_handle m_mesh_buffer_per_instance;
        gl_render_pass m_render_pass;        
    };

}