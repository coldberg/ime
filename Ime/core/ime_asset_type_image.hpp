#pragma once

#include "ime_asset_base.hpp"
#include "ime_image_format.hpp"

namespace ime {

    struct asset_image final:
        public asset_base_t<asset_image, asset_blob_base>
    {
        const void* data () const override;
        std::size_t size () const override;

        std::uint32_t width () const;
        std::uint32_t height () const;

        std::uint32_t stride () const;
        std::uint32_t bits_per_pixel () const;

        const image_format_descriptor& format () const;

        asset_image (std::unique_ptr<std::uint8_t []> s_data, 
                     std::uint32_t s_width, 
                     std::uint32_t s_height, 
                     const image_format_descriptor& s_format);
        ~asset_image () override = default;

    private:
        std::unique_ptr<std::uint8_t []> m_data;
        std::uint32_t m_width;
        std::uint32_t m_height;
        const image_format_descriptor& m_format;
    };

    typedef std::unique_ptr<const asset_image> asset_image_unique;
    typedef std::shared_ptr<const asset_image> asset_image_shared;
}
