#pragma once

#include <algorithm>
#include <functional>
#include <cstddef>

namespace ime {

    struct call_on_construct {        
        template <typename _Function>
        call_on_construct (const _Function&& s_fun) {
            s_fun ();
        }
    };

    struct call_on_exit {
        template <typename _Function>
        call_on_exit (_Function&& s_fun):
            m_call ([s_fun = std::forward<_Function> (s_fun)] () { s_fun (); })
        {}

        ~call_on_exit () {
            m_call ();
        }

        void cancel () {
            m_call = [] () {};
        }

        template <typename _Function>
        call_on_exit& operator = (_Function&& s_fun) {
            m_call = [s_fun = std::forward<_Function> (s_fun)] () { s_fun (); };
            return *this;
        }

    private:
        std::function<void ()> m_call;
    };

    template <typename T>
    inline auto duplicate_memory (const T* s_data, std::size_t s_size) {
        auto s_copy = std::make_unique<T []> (s_size);
        std::copy (s_data, s_data + s_size, s_copy.get ());
        return s_copy;
    }
    
    template <typename T>
    inline auto duplicate_text (const T* s_data, std::size_t s_size, bool s_fix_new_lines = false) {
        auto s_copy = std::make_unique<T []> (s_size + 1u);
        if (s_fix_new_lines == false) {
            std::copy (s_data, s_data + s_size, s_copy.get ());
            const_cast<T&> (s_copy [s_size]) = T (0);
        }
        else {
            const auto* e = s_data + s_size;
            auto* o = s_copy.get ();            
            for (const auto* i = s_data; i != e; ++i, ++o) {
                if (i [0] == '\r') {
                    if (e - i <= 1u || i [1] != '\n') {
                        o [0] = '\n';                        
                        continue;
                    }
                    ++i;
                }
                o [0] = i [0];
            }
            o [0] = '\0';
        }
        return s_copy;
    }

    inline auto to_hex (std::uint64_t s_value) {
        char s_buffer [24];
        std::sprintf (s_buffer, "%016llX", s_value);
        return std::string (s_buffer);
    }

    inline auto to_hex (const void* s_value) {
        return to_hex ((std::uintptr_t)s_value);
    }

    template <typename _Iter, typename _Function>
    inline void for_each (const std::pair<_Iter, _Iter>& s_range, _Function&& s_call) {
        std::for_each (s_range.first, s_range.second, std::forward<_Function> (s_call));
    }    

    template <typename _Callbacks, typename _Prefix, typename _Ptree, typename _Params>
    inline void iterate_tree_recursive (const _Callbacks& st_property_callback, const _Prefix& s_prefix, const _Ptree& s_root, _Params& s_params) {
        for (const auto& s_keyval : s_root) {
            auto s_key = s_prefix + s_keyval.first;
            try {
                st_property_callback.at (s_key) (s_params, s_keyval.second);
                iterate_tree_recursive (st_property_callback, s_key + ".", s_keyval.second, s_params);
            }
            catch (const std::out_of_range&) {
                ime::log.warn (__FILE__ ": invalid property or value for key `%s`.", s_key.c_str ());
            }
        }
    }

    template <typename _Callbacks, typename _Prefix, typename _Ptree, typename _Params>
    inline void iterate_tree (const _Callbacks& st_property_callback, const _Prefix& s_prefix, const _Ptree& s_root, _Params& s_params) {
        for (const auto& s_keyval : s_root) {
            auto s_key = s_prefix + s_keyval.first;
            try {
                st_property_callback.at (s_key) (s_params, s_keyval.second);                
            }
            catch (const std::out_of_range&) {
                ime::log.warn (__FILE__ ": invalid property or value for key `%s`.", s_key.c_str ());
            }
        }
    }

    template <typename _Callbacks, typename _Ptree, typename _Params, typename _PropName>
    inline void process_properties (const _Callbacks& st_property_callback, const _Ptree& s_root, _Params& s_params, const _PropName& s_prop_name) {
        const auto& s_prop = s_root.get_child_optional (s_prop_name);
        if (s_prop.is_initialized ()) {
            iterate_tree_recursive (st_property_callback, std::string (), *s_prop, s_params);
        }
    };


    #define IME_CONCAT0(X, Y) X##Y
    #define IME_CONCAT1(X, Y) IME_CONCAT0(X, Y)
    #define IME_BEFORE_MAIN static volatile ::ime::call_on_construct IME_CONCAT1(ime_call_before_main_, __LINE__) 
    #define IME_EXIT_GUARD  ::ime::call_on_exit IME_CONCAT1(ime_exit_guard_, __LINE__) 
}
