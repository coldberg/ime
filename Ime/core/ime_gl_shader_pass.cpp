#include "ime_gl_shader_pass.hpp"
#include <utility>

ime::gl_shader_pass::gl_shader_pass (ime::gl_program_handle s_handle, ime::gl_shader_props s_params):
    m_handle (std::move (s_handle)),
    m_params (std::move (s_params))
{}

ime::gl_shader_pass::gl_shader_pass (gl_shader_pass&& s_prev) :
    m_handle (std::move (s_prev.m_handle)),
    m_params (std::move (s_prev.m_params))
{}

ime::gl_shader_pass& ime::gl_shader_pass::operator = (gl_shader_pass&& s_prev) {
    (*this).~gl_shader_pass ();
    m_handle = std::move (s_prev.m_handle);
    m_params = std::move (s_prev.m_params);
    return *this;
}

ime::gl_shader_pass::~gl_shader_pass () {
}

const ime::gl_program_handle& ime::gl_shader_pass::handle () const {
    return m_handle;
}

const ime::gl_shader_props& ime::gl_shader_pass::props () const {
    return m_params;
}
