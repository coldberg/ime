#include "ime_gl_shader_props.hpp"

const ime::gl_shader_props& ime::gl_shader_props::default_props () {
    static const ime::gl_shader_props s_default_params;

    return s_default_params;
}
