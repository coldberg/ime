#pragma once

#include <cstddef>
#include <cstdint>

#include "ime_image_format.hpp"

namespace ime {

    struct gl_texture_format_descriptor {
        gl_texture_format_descriptor (
            std::uint32_t s_internal_format,
            std::uint32_t s_base_format,
            std::uint32_t s_channel_format,
            std::uint32_t r_swizzle,
            std::uint32_t g_swizzle,
            std::uint32_t b_swizzle,
            std::uint32_t a_swizzle);
        
        static const gl_texture_format_descriptor& from_image_format_descriptor (const image_format_descriptor&) noexcept;

        auto internal_format () const { return m_internal_format; }
        auto base_format     () const { return m_base_format; }
        auto channel_format  () const { return m_channel_format; }
        auto r_swizzle       () const { return m_r_swizzle; }
        auto g_swizzle       () const { return m_g_swizzle; }
        auto b_swizzle       () const { return m_b_swizzle; }
        auto a_swizzle       () const { return m_a_swizzle; }

    private:
        std::uint32_t m_internal_format;
        std::uint32_t m_base_format;        
        std::uint32_t m_channel_format;
        std::uint32_t m_r_swizzle;
        std::uint32_t m_g_swizzle;
        std::uint32_t m_b_swizzle;
        std::uint32_t m_a_swizzle;
    };

}