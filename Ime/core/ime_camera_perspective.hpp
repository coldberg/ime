#pragma once

#include "ime_camera.hpp"
#include "ime_frustrum.hpp"

namespace ime {

    struct camera_perspective: 
        public camera 
    {
        camera_perspective (const ime::mat3& m_frame, float m_aspect, float m_fov, float m_near, float m_far);

        const ime::mat3& frame () const;
        const float& aspect () const;
        const float& fov () const;
        const float& near () const;
        const float& far () const { return m_far; }

        void frame (const ime::mat3& s_value);
        void aspect (const float& s_value);
        void fov (const float& s_value);
        void near (const float& s_value);
        void far (const float& s_value);

        ime::vec3 upward () const;
        ime::vec3 forward () const;
        ime::vec3 rightward () const;

        void move (const vec3& m_offset);
        void rotate (const quat& m_offset);

    protected:
        void dirty_view ();
        void dirty_projection ();

    private:
        ime::mat3 m_frame;
        float m_aspect;
        float m_fov;
        float m_near;
        float m_far;
    };


}
