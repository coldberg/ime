#include "ime_asset_type_ptree.hpp"

ime::asset_ptree::asset_ptree (boost::property_tree::ptree s_ptree, ptree_type s_ptree_type):
    m_ptree (std::move (s_ptree)),
    m_ptree_type (s_ptree_type)
{}

const boost::property_tree::ptree& ime::asset_ptree::ptree () const { 
    return m_ptree; 
}

ime::asset_ptree::ptree_type ime::asset_ptree::ptree_type_hint () const {
    return m_ptree_type;
}
