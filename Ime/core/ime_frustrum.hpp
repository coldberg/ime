#pragma once

#include "ime_math.hpp"

namespace ime {
    struct frustrum {
        ime::vec3 m_position;
        ime::vec3 m_forward;
        ime::vec3 m_upward;

        struct {
            float m_distance;
            float m_width;
            float m_height;
        } m_near, m_far;
    };

}
