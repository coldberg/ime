#include "ime_gl_asset_shader.hpp"

ime::gl_asset_shader::gl_asset_shader (std::string s_name, std::vector<gl_shader_pass> s_passes, gl_shader_props s_params):
    m_passes (std::move (s_passes)),
    m_params (std::move (s_params)),
    m_name (std::move (s_name))
{}

ime::gl_asset_shader::~gl_asset_shader () {}

const std::vector<ime::gl_shader_pass>& ime::gl_asset_shader::passes () const {
    return m_passes;
}

const std::string& ime::gl_asset_shader::name () const {
    return m_name;
}

const ime::gl_shader_props& ime::gl_asset_shader::props () const {
    return m_params;
}
