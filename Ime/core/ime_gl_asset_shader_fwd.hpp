#pragma once 

#include <memory>

namespace ime {
    struct gl_asset_shader;

    typedef std::shared_ptr<const gl_asset_shader> gl_asset_shader_shared;
    typedef std::unique_ptr<const gl_asset_shader> gl_asset_shader_unique;
    typedef std::weak_ptr  <const gl_asset_shader> gl_asset_shader_weak;
}