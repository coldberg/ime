#include "ime_asset_type_image.hpp"

std::uint32_t ime::asset_image::width () const {
    return m_width;
}

std::uint32_t ime::asset_image::height () const {
    return m_height;
}

const void* ime::asset_image::data () const {
    return m_data.get ();
}

std::size_t ime::asset_image::size () const {
    return stride () * height ();
}

std::uint32_t ime::asset_image::stride () const {
    return (bits_per_pixel () * width ()) >> 3;
}

std::uint32_t ime::asset_image::bits_per_pixel () const {
    return m_format.size_in_bits ();
}

const ime::image_format_descriptor& ime::asset_image::format () const {
    return m_format;
}

ime::asset_image::asset_image (std::unique_ptr<std::uint8_t []> s_data, 
                               std::uint32_t s_width, 
                               std::uint32_t s_height, 
                               const ime::image_format_descriptor& s_format)
:   m_data   (std::move (s_data)),
    m_width  (s_width),
    m_height (s_height),
    m_format (s_format)
{
}

