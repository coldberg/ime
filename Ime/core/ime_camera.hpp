#pragma once

#include "ime_math.hpp"
#include <tuple>

namespace ime {

    struct camera {
        camera () = default;

        camera (const camera&) = default;
        camera& operator = (const camera&) = default;
        camera (camera&&) = default;
        camera& operator = (camera&&) = default;

        virtual ~camera () = default;

        const ime::mat4& view () const;
        const ime::mat4& projection () const;
        ime::mat4 projection_view () const;

        const std::pair<ime::vec2, ime::vec2>& viewport () const;
        const std::pair<ime::vec2, ime::vec2>& scissor () const;
        void viewport (const std::pair<ime::vec2, ime::vec2>& s_value);
        void scissor (const std::pair<ime::vec2, ime::vec2>& s_value);
      
    protected:
        void cache_view (const ime::mat4&) const;
        void cache_projection (const ime::mat4&) const;

    private:
        mutable ime::mat4 m_view;
        mutable ime::mat4 m_projection;

        std::pair<ime::vec2, ime::vec2> m_viewport = {{0, 0}, {1, 1}};
        std::pair<ime::vec2, ime::vec2> m_scissor = {{0, 0}, {1, 1}};
    };

};