#include "ime_asset_cache.hpp"
#include "ime_asset_locator.hpp"
#include "ime_asset_locator_file.hpp"
#include "ime_asset_locator_data.hpp"
#include "ime_asset_loader.hpp"
#include "ime_mime.hpp"

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <unordered_map>

#include <iostream>

struct asset_curator_internal: 
    public ime::asset_cache_interface 
{
    ime::asset_shared fetch (const std::string& s_path, const std::string& s_override_type) override {
        auto s_actual_path = s_path;

        const auto s_locator_it = m_asset_locator.find (s_actual_path);                
        const ime::asset_locator* s_locator = nullptr;
        if (s_locator_it != m_asset_locator.end ()) {
            s_locator = s_locator_it->second.get ();
        }

        //const auto s_file_locator_code = typeid (ime::asset_locator_file).hash_code ();
        //if (s_locator != nullptr && typeid (*s_locator).hash_code () == s_file_locator_code) {
        //    s_actual_path = s_locator->location ();
        //}

        auto& s_asset_slot = m_asset_cache [s_actual_path];

        if (s_asset_slot.use_count () > 0u) {
            return s_asset_slot.lock ();
        }

        ime::log.trace ("Loading asset `%s`... ", s_path.c_str ());

        if (s_locator != nullptr) {
            const auto s_mime_type = s_override_type != "" ? s_override_type : s_locator->type ();
            auto& s_loader = ime::asset_loader::locate (s_mime_type);
            auto s_asset = s_loader.load (s_path, *s_locator);
            s_asset_slot = s_asset;
            return s_asset;
        }

        using namespace boost::filesystem;
        if (exists (path (s_actual_path))) {
            const auto s_mime_type = s_override_type != "" ? s_override_type
                : ime::mime_type_from_file_extension (s_actual_path);
            auto& s_loader = ime::asset_loader::locate (s_mime_type);
            auto s_asset = s_loader.load (s_actual_path, ime::asset_locator_file (s_actual_path, s_mime_type));
            s_asset_slot = s_asset;
            return s_asset;
        }

        return nullptr;
    }

    bool map_asset (const std::string& s_path, ime::asset_locator_unique s_locator) override {
        auto& s_slot = m_asset_locator [s_path];
        auto s_result = (s_slot == nullptr);
        s_slot = std::move (s_locator);
        return s_result;
    }

    bool unmap_asset (const std::string& s_path) override {
        return m_asset_locator.erase (s_path) != 0;
    }

private:
    std::unordered_map<std::string, ime::asset_locator_unique> m_asset_locator;
    std::unordered_map<std::string, ime::asset_weak> m_asset_cache;
};



namespace ime {       
    asset_cache_interface& asset_cache () {
        static asset_curator_internal st_curator;
        return st_curator;
    }
}
