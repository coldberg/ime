#include <unordered_map>
#include <boost/filesystem/path.hpp>

#include "ime_mime.hpp"

std::string ime::mime_type_from_file_extension (const std::string & s_path) {

    static const std::unordered_map<std::string, std::string> st_mime_map = {

        {".bin",     default_mime_type},
        {".txt",     "text/plain"},

        {".vert",    "text/plain"},
        {".tesc",    "text/plain"},
        {".tese",    "text/plain"},
        {".geom",    "text/plain"},
        {".frag",    "text/plain"},
        {".comp",    "text/plain"},

        {".json",    "text/json"},
        {".info",    "text/info"},
        {".xml",     "text/xml"},
        {".ini",     "text/ini"},
        {".prop",    "text/ptree"},

        {".png",     "image/png"},
        {".tga",     "image/tga"},
        {".jpg",     "image/jpg"},

        {".texture", "glcore/texture"},
        {".shader",  "glcore/shader"},
        
    };

    using namespace boost::filesystem;
    auto s_ext = path (s_path).extension ().string ();

    if (st_mime_map.count (s_ext)) {
        return st_mime_map.at (s_ext);
    }
    return default_mime_type;
}
