#pragma once

#include <utility>
#include <cstdint>

#include "ime_gl_context.hpp"
#include "ime_gl_handle_fwd.hpp"

namespace ime {

    template <typename _Handle, typename _Tag>
    inline gl_handle_non_copyable<_Handle, _Tag>::gl_handle_non_copyable (gl_handle_non_copyable&& s_prev):
        m_value (std::exchange (s_prev.m_value, 0u)),
        m_api (std::move (s_prev.m_api)) 
    {}

    template <typename _Handle, typename _Tag>
    inline gl_handle_non_copyable<_Handle, _Tag>& gl_handle_non_copyable<_Handle, _Tag>::operator = (gl_handle_non_copyable&& s_prev) {
        (*this).~gl_handle_non_copyable ();
        m_value = std::exchange (s_prev.m_value, 0u);
        m_api = std::move (s_prev.m_api);
        return *this;
    }


    template <typename _Handle, typename _Tag>
    inline gl_handle_non_copyable<_Handle, _Tag>::gl_handle_non_copyable (const value_type& s_value, ime::gl_context_weak s_api):
        m_value (s_value),
        m_api (std::move (s_api)) 
    {}

    template <typename _Handle, typename _Tag>
    inline gl_handle_non_copyable<_Handle, _Tag>::~gl_handle_non_copyable () {
        if (m_value == value_type ()) {
            return;
        }
        if (m_api.use_count () > 0) {
            auto s_api = m_api.lock ();
            s_api->release (*this);
            m_value = 0u;
            return;
        }
        ime::log.warn (__FILE__ ": context has gone away , can't release resource.");
    }

    template <typename _Handle, typename _Tag>
    inline typename gl_handle_non_copyable<_Handle, _Tag>::value_type 
        gl_handle_non_copyable<_Handle, _Tag>::value () const 
    {
        return m_value;
    }

    template <typename _Handle, typename _Tag>
    inline const ime::gl_context_weak& gl_handle_non_copyable<_Handle, _Tag>::api () const {
        return m_api;
    }

}