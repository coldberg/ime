#pragma once

#include <cstdio>
#include <functional>

namespace ime {

    struct log_base {
        template <typename _Ctype, std::size_t _Length, typename... Args> void fatal (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("  FATAL: ", stderr); std::fprintf (stderr, fmt, std::forward<Args> (args)...); std::fputc ('\n', stderr); }
        template <typename _Ctype, std::size_t _Length, typename... Args> void error (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("  ERROR: ", stderr); std::fprintf (stderr, fmt, std::forward<Args> (args)...); std::fputc ('\n', stderr); }
        template <typename _Ctype, std::size_t _Length, typename... Args> void warn  (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("WARNING: ", stderr); std::fprintf (stderr, fmt, std::forward<Args> (args)...); std::fputc ('\n', stderr); }
        template <typename _Ctype, std::size_t _Length, typename... Args> void info  (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("   INFO: ", stdout); std::fprintf (stdout, fmt, std::forward<Args> (args)...); std::fputc ('\n', stdout); }
        template <typename _Ctype, std::size_t _Length, typename... Args> void debug (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("  DEBUG: ", stdout); std::fprintf (stdout, fmt, std::forward<Args> (args)...); std::fputc ('\n', stdout); }
        template <typename _Ctype, std::size_t _Length, typename... Args> void trace (const _Ctype (&fmt) [_Length], Args&&... args) { std::fputs ("  TRACE: ", stdout); std::fprintf (stdout, fmt, std::forward<Args> (args)...); std::fputc ('\n', stdout); }
    };

    extern log_base& log;
}
