#pragma once

#include <string>



namespace ime {
    static constexpr char default_mime_type [] = "application/octet-stream";

    std::string mime_type_from_file_extension (const std::string& s_path);

}
