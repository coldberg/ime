#pragma once

#include "ime_gl_shader_props.hpp"
#include "ime_gl_handle.hpp"

namespace ime {

    
    struct gl_shader_pass {

        gl_shader_pass (gl_program_handle s_handle, gl_shader_props s_params = gl_shader_props::default_props ());

        gl_shader_pass (const gl_shader_pass&) = delete;
        gl_shader_pass& operator = (const gl_shader_pass&) = delete;
        gl_shader_pass (gl_shader_pass&&);
        gl_shader_pass& operator = (gl_shader_pass&&);

        ~gl_shader_pass ();

        const gl_program_handle& handle () const;
        const gl_shader_props& props () const;

    private:
        gl_program_handle m_handle;        
        gl_shader_props   m_params;
    };
}