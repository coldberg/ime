#pragma once

//#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace ime {
    using namespace glm;

    typedef tquat<int> iquat;
    typedef tquat<unsigned> uquat;
}