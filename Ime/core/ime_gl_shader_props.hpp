#pragma once

#include "ime_gl_enum.hpp"

namespace ime {
    struct gl_shader_props {
        static const gl_shader_props& default_props ();

        struct {
            bool test = false;
            bool write = false;
        }depth;

        struct {
            bool enabled = false;
            gl_blend_func_enum_type src_factor = gl_blend_func_one;
            gl_blend_func_enum_type dst_factor = gl_blend_func_zero;
            gl_blend_equation_enum_type equation = gl_blend_equation_add;
        }blend;
    };
}