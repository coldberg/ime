#include "ime_asset_loader_text.hpp"
#include "ime_asset_type_text.hpp"
#include "ime_utils.hpp"
#include "ime_log.hpp"

ime::asset_shared ime::asset_loader_text::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_file = s_locator.access ();
    if (s_file->is_good ()) {
        auto s_size = s_file->size ();
        auto s_data = s_file->map_read_only (0u, s_size);
        if (!s_file->is_good ()) {
            ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
            return nullptr;
        }
        auto s_copy = ime::duplicate_text ((const char *)s_data, s_size, true);
        return asset_text::new_shared (std::move (s_copy), s_size);
    }
    return nullptr;
}

ime::asset_loader_text::asset_loader_text () {
    ime::asset_loader::register_loader ("text/plain",
    [this] (const std::string& s_mine_type)
         -> ime::asset_loader& 
    {        
        return *this;
    });
}

static ime::asset_loader_text st_asset_loader_text;

