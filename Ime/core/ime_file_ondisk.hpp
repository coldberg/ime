#pragma once

#include <string>
#include <boost/iostreams/device/mapped_file.hpp>

#include "ime_file_base.hpp"
#include "ime_file_buffer.hpp"

namespace ime {

    struct immutable_file_ondisk: 
        public immutable_file_buffer 
    {
        immutable_file_ondisk (const std::string& s_path);
        ~immutable_file_ondisk () override = default;
    private:
        boost::iostreams::mapped_file_source m_file;
    };

}
