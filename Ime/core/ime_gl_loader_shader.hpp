#pragma once

#include "ime_asset_loader.hpp"
#include <boost/property_tree/ptree.hpp>

namespace ime {

    struct gl_core_shader_loader:
        public ime::asset_loader 
    {
        gl_core_shader_loader () = default;
        ~gl_core_shader_loader () override;
        asset_shared load (const std::string& s_key, const asset_locator&) const override;
        asset_shared load (const std::string& s_key, const boost::property_tree::ptree&) const;
    };



}
