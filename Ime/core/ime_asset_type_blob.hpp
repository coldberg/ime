#pragma once

#include "ime_asset_base.hpp"

namespace ime {

    struct asset_blob final:
        public ime::asset_base_t<asset_blob, asset_blob_base> 
    {
        asset_blob (std::unique_ptr<std::uint8_t []> s_data, std::size_t s_size);

        const void* data () const override;
        std::size_t size () const override;

    private:
        std::unique_ptr<std::uint8_t []> m_data;
        std::size_t m_size;
    };

    typedef std::shared_ptr<asset_blob> asset_text_shared;

}

