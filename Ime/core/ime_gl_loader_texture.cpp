#include "ime_asset_cache.hpp"
#include "ime_asset_loader.hpp"
#include "ime_asset_type_ptree.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_gl_context.hpp"
#include "ime_gl_texture_format.hpp"
#include "ime_gl_loader_texture.hpp"
#include "ime_gl_asset_texture.hpp"
#include "ime_utils.hpp"


#include <sstream>
#include <unordered_map>
#include <array>

ime::gl_core_texture_loader::~gl_core_texture_loader () {}


ime::asset_shared ime::gl_core_texture_loader::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_ptree_asset = ime::plain_load_as_<ime::asset_ptree> (s_key, s_locator, "text/ptree");
    if (s_ptree_asset == nullptr) {
        log.warn (__FILE__ ": unable to load texture properties.");
        return nullptr;
    }
    
    const auto& s_tree = s_ptree_asset->ptree ().get_child_optional ("texture");
    if (s_tree.is_initialized ()) {
        return load (s_key, *s_tree);
    }
    ime::log.warn (__FILE__ ": unable to parse \"%s\".", s_locator.location ());
    return nullptr;
}

static void copy_sub_image (std::uint8_t* s_dst_data, const ime::asset_image& s_src, const ime::uvec2& s_offset, const ime::uvec2& s_extent) {
    const auto s_src_stride = s_src.stride ();
    const auto s_src_offset = s_src.format ().size_in_bytes () * s_offset.x + s_src_stride * s_offset.y;
    const auto s_dst_stride = s_src.format ().size_in_bytes () * s_extent.x;
    const auto s_sline_size = s_src.format ().size_in_bytes () * std::min (s_src.width () - s_offset.x, s_extent.x);
    const auto s_height_max = std::min (s_src.height () - s_offset.y, s_extent.y);
    const auto* s_src_data = (const std::uint8_t*)s_src.data () + s_src_offset;

    for (auto j = 0u; j < s_height_max; ++j) {
        std::memcpy (s_dst_data + j*s_dst_stride, 
                    s_src_data  + j*s_src_stride, 
                    s_sline_size);
    }

}

ime::asset_shared ime::gl_core_texture_loader::load (const std::string& s_key, const boost::property_tree::ptree& s_tree) const {
    struct texture_parameters {

        gl_texture_target_enum_type target = gl_enum_texture2d;
        gl_texel_format_enum_type format = gl_texel_format_dont_care;
        std::string source = "";
        struct {
            std::uint32_t vertical = 1;
            std::uint32_t horizontal = 1;
        }
        slice;

        struct {
            struct {
                gl_interpolation_mode_enum_type minification = gl_enum_nearest;
                gl_interpolation_mode_enum_type magnification = gl_enum_nearest;
                std::uint32_t max_mipmap_level = 1000;
                std::int32_t lod_min = -1000;
                std::int32_t lod_bias = 0;
                std::int32_t lod_max = 1000;
            } filter;

            struct {
                gl_texture_wrap_enum_type s = gl_enum_repeat;
                gl_texture_wrap_enum_type t = gl_enum_repeat;
                gl_texture_wrap_enum_type r = gl_enum_repeat;
            } wrap;

            ime::vec4 border_color = ime::vec4 (0);
        }
        sampler;
    };

    typedef std::function<void (texture_parameters& s_texture, const boost::property_tree::ptree& s_child)> key_value_handler_type;
    static const std::unordered_map<std::string, key_value_handler_type> st_texture_handler = {

        {"format", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.format = ime::gl_enum_cast<gl_texel_format_enum_type> (s_child.get_value<std::string> ());
        }},

        {"target", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.target = ime::gl_enum_cast<gl_texture_target_enum_type> (s_child.get_value<std::string> ());
        }},

        {"source", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.source = s_child.get_value<std::string> ();
        }},

        {"slice", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {

        }},

        {"slice.horizontal", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.slice.horizontal = s_child.get_value<std::uint32_t> ();
        }},

        {"slice.vertical", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.slice.vertical = s_child.get_value<std::uint32_t> ();
        }},

        {"sampler", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {}},

        {"sampler.filter", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {}},

        {"sampler.filter.magnification", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.magnification = ime::gl_enum_cast<gl_interpolation_mode_enum_type> (s_child.get_value<std::string> ());
        }},
        {"sampler.filter.minification", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.minification = ime::gl_enum_cast<gl_interpolation_mode_enum_type> (s_child.get_value<std::string> ());
        }},
        {"sampler.filter.max_mipmap_level", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.max_mipmap_level = s_child.get_value<std::int32_t> ();
        }},

        {"sampler.filter.lod_min", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.lod_min = s_child.get_value<std::int32_t> ();
        }},
        {"sampler.filter.lod_bias", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.lod_bias = s_child.get_value<std::int32_t> ();
        }},
        {"sampler.filter.lod_max", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.filter.lod_max = s_child.get_value<std::int32_t> ();
        }},

        {"sampler.wrap", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {}},

        {"sampler.wrap.s", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.wrap.s = ime::gl_enum_cast<gl_texture_wrap_enum_type> (s_child.get_value<std::string> ());
        }},

        {"sampler.wrap.t", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.wrap.t = ime::gl_enum_cast<gl_texture_wrap_enum_type> (s_child.get_value<std::string> ());
        }},

        {"sampler.wrap.r", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.wrap.r = ime::gl_enum_cast<gl_texture_wrap_enum_type> (s_child.get_value<std::string> ());
        }},

        {"sampler.border_color", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {

        }},

        {"sampler.border_color.r", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.border_color.r = s_child.get_value<float> ();
        }},

        {"sampler.border_color.g", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.border_color.g = s_child.get_value<float> ();
        }},

        {"sampler.border_color.b", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.border_color.b = s_child.get_value<float> ();
        }},

        {"sampler.border_color.a", [] (texture_parameters& s_texture, const boost::property_tree::ptree& s_child) {
            s_texture.sampler.border_color.a = s_child.get_value<float> ();
        }},

    };

    auto s_core = ime::gl_context::lock ();
    if (s_core == nullptr) {
        log.warn (__FILE__ ": unable to load texture, gl_core not initialized.");
        return nullptr;
    }

    texture_parameters s_params;
    ime::iterate_tree_recursive (st_texture_handler, "", s_tree, s_params);

    auto s_source = ime::asset_cache ().fetch_as_<ime::asset_image> (s_params.source);
    if (s_source == nullptr) {
        ime::log.warn (__FILE__ ": unable to load `%s`.", s_params.source.c_str ());
        return nullptr;
    }

    gl_texture_handle s_texture_handle;

    const auto s_slice_width = (std::uint32_t)std::ceil (1.0f*s_source->width () / s_params.slice.horizontal);
    const auto s_slice_height = (std::uint32_t)std::ceil (1.0f*s_source->height () / s_params.slice.vertical);
    const auto s_slice_number = s_params.slice.horizontal * s_params.slice.vertical;

    switch (s_params.target) {
        case gl_enum_texture2d_array:
        case gl_enum_texture2d_cube:
        case gl_enum_texture2d_cube_array:
        {
            auto s_source_extent = uvec2 (s_slice_width, s_slice_height);

            auto s_extent = uvec2 (std::max (s_source_extent.x, s_source_extent.y));
            auto s_offset = uvec2 ((s_extent.x - s_source_extent.x)/2, (s_extent.y - s_source_extent.y)/2);

            auto s_stride = s_source->format ().size_in_bytes ()*s_source_extent.x;
            auto s_buffer = std::make_unique<std::uint8_t []> (s_stride * s_source_extent.y);

            s_texture_handle = s_core->texture_create (s_params.target, s_params.format, uvec3 (s_extent, s_slice_number), 0);

            for (auto j = 0u; j < s_params.slice.vertical; ++j) {
                for (auto i = 0u; i < s_params.slice.horizontal; ++i) {
                    copy_sub_image (s_buffer.get (), *s_source, uvec2 (i, j)*s_extent, s_extent);
                    const auto s = s_params.slice.horizontal * j + i;
                    s_core->texture_update (s_texture_handle, s_params.target, uvec3 (s_offset, s), uvec3 (s_extent, 1u), s_buffer.get (), s_source->format (), 0u);
                }
            }
            break;
        }
        case gl_enum_texture3d:        
        {
            auto s_source_extent = uvec3 (s_slice_width, s_slice_height, s_slice_height);

            auto s_extent = uvec3 (std::max ({s_slice_width, s_slice_height, s_slice_number}));
            auto s_offset = uvec3 ((s_extent.x - s_source_extent.x)/2,
                                   (s_extent.y - s_source_extent.y)/2,
                                   (s_extent.z - s_source_extent.z)/2);

            auto s_stride = s_source->format ().size_in_bytes ()*s_source_extent.x;
            auto s_buffer = std::make_unique<std::uint8_t []> (s_stride * s_source_extent.y);

            s_texture_handle = s_core->texture_create (s_params.target, s_params.format, s_extent, 0);

            for (auto j = 0u; j < s_params.slice.vertical; ++j) {
                for (auto i = 0u; i < s_params.slice.horizontal; ++i) {
                    copy_sub_image (s_buffer.get (), *s_source, uvec2 (i, j)*uvec2 (s_extent.x, s_extent.y), uvec2 (s_extent.x, s_extent.y));
                    const auto s = s_params.slice.horizontal * j + i;
                    s_core->texture_update (s_texture_handle, s_params.target, s_offset + uvec3 (0, 0, s), uvec3 (s_extent.x, s_extent.y, 1u), s_buffer.get (), s_source->format (), 0u);
                }
            }
            break;
        }
        case gl_enum_texture2d:
        {
            auto s_source_extent = uvec3 (s_source->width (), s_source->height (), 1);

            auto s_extent = uvec3 (uvec2 (std::max (s_source_extent.x, s_source_extent.y)), 1u);
            auto s_offset = uvec3 ((s_extent.x - s_source_extent.x)/2, 
                                   (s_extent.y - s_source_extent.y)/2, 0);

            s_texture_handle = s_core->texture_create (s_params.target, s_params.format, s_extent, 1000u);
            s_core->texture_update (s_texture_handle, s_params.target, s_offset, s_source_extent, s_source->data (), s_source->format (), 0u);
            break;
        }
        case gl_enum_texture2d_rectangle:
        case gl_enum_texture1d_array:
        {
            auto s_extent = uvec3 (s_source->width (), s_source->height (), 1);
            s_texture_handle = s_core->texture_create (s_params.target, s_params.format, s_extent, 1000u);
            s_core->texture_update (s_texture_handle, s_params.target, uvec3 (0), s_extent, s_source->data (), s_source->format (), 0u);
            break;
        }
        case gl_enum_texture1d:
        {
            auto s_extent = uvec3 (s_source->width () * s_source->height (), 1, 1);
            s_texture_handle = s_core->texture_create (s_params.target, s_params.format, s_extent, 1000u);
            s_core->texture_update (s_texture_handle, s_params.target, uvec3 (0), s_extent, s_source->data (), s_source->format (), 0u);
            break;
        }
        default:
        {
            ime::log.warn (__FILE__ ": target `%s` not supported.", s_tree.get_child ("target").get_value<std::string> ().c_str ());
            break;
        }
    }

    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_min_filter,  (std::int32_t)s_params.sampler.filter.minification);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_mag_filter,  (std::int32_t)s_params.sampler.filter.magnification);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_min_lod,     (std::int32_t)s_params.sampler.filter.lod_min);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_lod_bias,    (std::int32_t)s_params.sampler.filter.lod_bias);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_max_lod,     (std::int32_t)s_params.sampler.filter.lod_max);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_wrap_s,      (std::int32_t)s_params.sampler.wrap.s);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_wrap_t,      (std::int32_t)s_params.sampler.wrap.t);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_wrap_r,      (std::int32_t)s_params.sampler.wrap.r);
    s_core->texture_parameter (s_texture_handle, gl_texture_parameter_border_color, s_params.sampler.border_color);

    s_core->texture_build_mipmaps (s_texture_handle);

    return gl_asset_texture::new_shared (std::move (s_texture_handle));
}

IME_BEFORE_MAIN ([] () {
    static ime::gl_core_texture_loader g_loader;
    ime::asset_loader::register_loader ("glcore/texture",
    [] (const std::string& s_type) -> ime::asset_loader& {
        return g_loader;
    });
});