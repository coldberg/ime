#pragma once

#include <memory>

#include "ime_asset_base.hpp"
#include "ime_gl_handle_fwd.hpp"
#include "ime_gl_asset_texture_fwd.hpp"

namespace ime {

    struct gl_asset_texture final:
        public asset_base_t<gl_asset_texture>
    {
        gl_asset_texture (gl_texture_handle s_texture);
        ~gl_asset_texture () override;

        const gl_texture_handle& handle () const { return m_texture; }

    private:
        gl_texture_handle m_texture;
    };

}
