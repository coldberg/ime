#include "ime_gl_texture_format.hpp"

#include <unordered_map>

#include <GL/glew.h>

static const std::unordered_map<ime::image_format_descriptor, ime::gl_texture_format_descriptor> gl_format_mapping = {
    {ime::image_format_argb16161616, {GL_RGBA16,  GL_RGBA, GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_abgr16161616, {GL_RGBA16,  GL_BGRA, GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_rgba16161616, {GL_RGBA16,  GL_RGBA, GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_bgra16161616, {GL_RGBA16,  GL_BGRA, GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},                                                                        
    {ime::image_format_argb8888,     {GL_RGBA8,   GL_RGBA, GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_abgr8888,     {GL_RGBA8,   GL_RGBA, GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_rgba8888,     {GL_RGBA8,   GL_RGBA, GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_bgra8888,     {GL_RGBA8,   GL_BGRA, GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_argb1555,     {GL_RGB5_A1, GL_RGBA, GL_UNSIGNED_SHORT_1_5_5_5_REV, GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_abgr1555,     {GL_RGB5_A1, GL_BGRA, GL_UNSIGNED_SHORT_1_5_5_5_REV, GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA}},
    {ime::image_format_rgb161616,    {GL_RGB16,   GL_RGB,  GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_bgr161616,    {GL_RGB16,   GL_BGR,  GL_UNSIGNED_SHORT,             GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_rgb888,       {GL_RGB8,    GL_RGB,  GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_bgr888,       {GL_RGB8,    GL_BGR,  GL_UNSIGNED_BYTE,              GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_rgb555,       {GL_RGB5,    GL_RGBA, GL_UNSIGNED_SHORT_1_5_5_5_REV, GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_bgr555,       {GL_RGB5,    GL_BGRA, GL_UNSIGNED_SHORT_1_5_5_5_REV, GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_rgb565,       {GL_RGB5,    GL_RGB,  GL_UNSIGNED_SHORT_5_6_5,       GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},
    {ime::image_format_bgr565,       {GL_RGB5,    GL_RGB,  GL_UNSIGNED_SHORT_5_6_5,       GL_RED, GL_GREEN, GL_BLUE, GL_ONE}},                                                                        
    {ime::image_format_ga1616,       {GL_RG16,    GL_RG,   GL_UNSIGNED_SHORT,             GL_RED, GL_RED,   GL_RED,  GL_ALPHA}},
    {ime::image_format_ag1616,       {GL_RG16,    GL_RG,   GL_UNSIGNED_SHORT,             GL_RED, GL_RED,   GL_RED,  GL_ALPHA}},
    {ime::image_format_ga88,         {GL_RG8,     GL_RG,   GL_UNSIGNED_BYTE,              GL_RED, GL_RED,   GL_RED,  GL_ALPHA}},
    {ime::image_format_ag88,         {GL_RG8,     GL_RG,   GL_UNSIGNED_BYTE,              GL_RED, GL_RED,   GL_RED,  GL_ALPHA}},                                                                        
    {ime::image_format_g16,          {GL_R16,     GL_RED,  GL_UNSIGNED_SHORT,             GL_RED, GL_RED,   GL_RED,  GL_ONE}},
    {ime::image_format_g8,           {GL_R8,      GL_RED,  GL_UNSIGNED_BYTE,              GL_RED, GL_RED,   GL_RED,  GL_ONE}}
};

ime::gl_texture_format_descriptor::gl_texture_format_descriptor (
    std::uint32_t s_internal_format,
    std::uint32_t s_base_format, 
    std::uint32_t s_type, 
    std::uint32_t r_swizzle, 
    std::uint32_t g_swizzle, 
    std::uint32_t b_swizzle, 
    std::uint32_t a_swizzle)
:   m_internal_format (s_internal_format),
    m_base_format (s_base_format),    
    m_channel_format (s_type),
    m_r_swizzle (r_swizzle),
    m_g_swizzle (g_swizzle),
    m_b_swizzle (b_swizzle),
    m_a_swizzle (a_swizzle) 
{}

const ime::gl_texture_format_descriptor& ime::gl_texture_format_descriptor::from_image_format_descriptor (const image_format_descriptor& s_ifd) noexcept {
    return gl_format_mapping.at (s_ifd);
}
