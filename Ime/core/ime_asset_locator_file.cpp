#include "ime_asset_locator_file.hpp"
#include "ime_asset_loader.hpp"
#include "ime_file_ondisk.hpp"
#include <memory>

ime::asset_locator_file::asset_locator_file (const std::string& s_file_path, const std::string & s_mime_type)
:   m_file_path (s_file_path),
    m_mime_type (s_mime_type) 
{}

ime::asset_locator_file::asset_locator_file (const std::string& s_file_path)
:   asset_locator_file (s_file_path, ime::mime_type_from_file_extension (s_file_path)) 
{}

const char* ime::asset_locator_file::type () const {
    return m_mime_type.c_str (); 
}

const char * ime::asset_locator_file::location () const {
    return file_path ();
}

const char* ime::asset_locator_file::file_path () const { 
    return m_file_path.c_str (); 
}

auto ime::asset_locator_file::access () const -> std::shared_ptr<ime::immutable_file_base> {
    return std::make_shared<ime::immutable_file_ondisk> (file_path ());
}
