#pragma once 

#include <cstddef>
#include <cstdint>

namespace ime {

    template <typename _Key, typename _Base = std::uint64_t>
    struct small_set {
        small_set (): m_bits (0u) {}
        small_set (const std::initializer_list<_Key>& s_all_keys):
            small_set ()
        {
            for (const auto& s_key : s_all_keys) {
                (*this).insert (s_key);
            }
        };

        void insert (const _Key& s_key) {
            m_bits |= (_Base (1) << _Base(s_key));
        }

        void erase (const _Key& s_key) {
            m_bits &= (~(_Base (1) << _Base(s_key)));
        }

        auto count (const _Key& s_key) const {
            return m_bits & (_Base (1) << _Base(s_key)) ? 1 : 0;
        }

        bool contains (const _Key& s_key) const {
            return count (s_key) > 0;
        }

        const auto& to_bits () const {
            return m_bits;
        }

    private:
        _Base m_bits;
    };

}