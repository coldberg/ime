#pragma once

#include <memory>
#include "ime_log.hpp"

namespace ime {

    struct asset_base : std::enable_shared_from_this<asset_base> {
        asset_base () = default;
        asset_base (const asset_base&) = delete;
        asset_base& operator = (const asset_base&) = delete;
        asset_base (asset_base&&) = delete;
        asset_base& operator = (asset_base&&) = delete;

        virtual ~asset_base () = default;
        template <typename _NewAssetType> 
        auto as_ () const {
            // TODO : test this function
            auto s_tmp = this->shared_from_this ();
            if (s_tmp != nullptr) {
                return std::dynamic_pointer_cast<const _NewAssetType> (s_tmp);
            }
            ime::log.warn (__FILE__ ": attempted to cast null to %s.", typeid (_NewAssetType).name ());
            return std::shared_ptr<const _NewAssetType> (nullptr);
        }
    };


    struct asset_blob_base: asset_base {
        virtual const void* data () const = 0;
        virtual std::size_t size () const = 0;
    };


    typedef std::shared_ptr<const asset_base> asset_shared;
    typedef std::unique_ptr<const asset_base> asset_unique;
    typedef std::weak_ptr<const asset_base> asset_weak;

    template <typename _SubClass, typename _SuperClass = asset_base>
    struct asset_base_t: public _SuperClass 
    {
        template <typename... Args>
        static auto new_shared (Args&&... args) {
            return std::make_shared<_SubClass> (std::forward<Args> (args)...);
        }

        template <typename... Args>
        static auto new_unique (Args&&... args) {
            return std::make_unique<_SubClass> (std::forward<Args> (args)...);
        }
    };

}
