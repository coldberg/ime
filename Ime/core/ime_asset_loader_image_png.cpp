#include "ime_asset_loader_image_png.hpp"
#include "ime_image_format.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_utils.hpp"
#include "ime_log.hpp"

#include <memory>

#include <png.h>

ime::asset_shared ime::asset_loader_image_png::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_file = s_locator.access ();
    if (!s_file || !s_file->is_good ()) {
        ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
        return nullptr;
    }

    png_byte s_signature [8u];

    if (s_file->read (s_signature, sizeof (s_signature)) != sizeof (s_signature) 
     || !png_check_sig (s_signature, sizeof (s_signature)))
    {
        ime::log.warn (__FILE__ ": failed verifying png signature, file not png or is corrupt.");
        return nullptr;
    }

    auto s_read_struct = png_create_read_struct (PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (s_read_struct == nullptr) {
        ime::log.warn (__FILE__ ": failed png_create_read_struct.");
        return nullptr;
    }

    IME_EXIT_GUARD ([&] () {         
        png_destroy_read_struct (&s_read_struct, nullptr, nullptr); 
    });

    auto s_info_struct = png_create_info_struct (s_read_struct);
    if (s_info_struct == nullptr) {
        ime::log.warn (__FILE__ ": failed png_create_info_struct.");
        return nullptr;
    }
        
    IME_EXIT_GUARD ([&] () {
        png_destroy_info_struct (s_read_struct, &s_info_struct);
    });
   
    png_set_read_fn (s_read_struct, s_file.get (),
    [] (png_struct* s_read_struct, 
        png_byte* s_buffer, 
        png_size_t s_bytes_to_read) 
        -> void 
    {
        auto s_file = (immutable_file_base*)png_get_io_ptr (s_read_struct);
        auto s_read = s_file->read (s_buffer, s_bytes_to_read);
        if (s_read != s_bytes_to_read) {
            ime::log.warn (__FILE__ ": premature end of file, read came up %d bytes short.", s_bytes_to_read - s_read);
        }
    });

    png_set_sig_bytes (s_read_struct, sizeof (s_signature));
    png_read_info (s_read_struct, s_info_struct);

    png_uint_32 s_width = 0;
    png_uint_32 s_height = 0;
    int s_color_type = 0;
    int s_bpp = 0;

    auto s_status = png_get_IHDR (
        s_read_struct, 
        s_info_struct, 
        &s_width, 
        &s_height,
        &s_bpp, 
        &s_color_type, 
        nullptr, 
        nullptr, 
        nullptr
    );

    if (s_status != 1) {
        ime::log.warn (__FILE__ ": unable to retrieve png information.");
        return nullptr;
    }

    const ime::image_format_descriptor* s_format = nullptr;  
    switch (s_color_type) {
        case PNG_COLOR_TYPE_RGB_ALPHA:
        {
            switch (s_bpp) {
                case  8: s_format = &ime::image_format_abgr8888; break;
                case 16: s_format = &ime::image_format_abgr16161616; break;
                default:
                {
                    ime::log.warn (__FILE__ ": bad bits per pixel and color type combination.");
                    return nullptr;
                }
            }
            break;
        }
        case PNG_COLOR_TYPE_RGB:
        {
            switch (s_bpp) {
                case  8: s_format = &ime::image_format_bgr888; break;
                case 16: s_format = &ime::image_format_bgr161616; break;
                default:
                {
                    ime::log.warn (__FILE__ ": bad bits per pixel and color type combination.");
                    return nullptr;
                }
            }
            break;
        }
        case PNG_COLOR_TYPE_GRAY_ALPHA:
        {
            switch (s_bpp) {
                case  8: s_format = &ime::image_format_ag88; break;
                case 16: s_format = &ime::image_format_ag1616; break;
                default:
                {
                    ime::log.warn (__FILE__ ": bad bits per pixel and color type combination.");
                    return nullptr;
                }
            }
            break;
        }
        case PNG_COLOR_TYPE_GRAY:
        {
            switch (s_bpp) {
                case  8: s_format = &ime::image_format_g8; break;
                case 16: s_format = &ime::image_format_g16; break;
                default:
                {
                    ime::log.warn (__FILE__ ": bad bits per pixel and color type combination.");
                    return nullptr;
                }
            }
            break;
        }
           
        default:
        {
            ime::log.warn (__FILE__ ": only rgb, rgb_alpha, gray and gray_alpha supported.");
            return nullptr;
        }
    }

    auto s_pixel_size = s_format->size_in_bytes ();
    auto s_row_stride = s_pixel_size * s_width;
    auto s_total_size = s_row_stride * s_height;
    auto s_image_data = std::make_unique<std::uint8_t []> (s_total_size);

    for (auto j = 0u; j < s_height; ++j) {
        png_read_row (s_read_struct, &s_image_data [j * s_row_stride], nullptr);
    }

    return asset_image::new_shared (std::move (s_image_data), s_width, s_height, *s_format);
}

ime::asset_loader_image_png::asset_loader_image_png () {
    ime::asset_loader::register_loader ("image/png",
    [this] (const std::string& s_mime_type)
        -> ime::asset_loader& 
    {
        return *this;
    });
}

static ime::asset_loader_image_png g_asset_loader_png;
