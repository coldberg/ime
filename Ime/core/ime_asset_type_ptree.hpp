#pragma once

#include <boost/property_tree/ptree.hpp>

#include <string>

#include "ime_asset_base.hpp"

namespace ime {
    struct asset_ptree final:
        public asset_base_t<asset_ptree> 
    {        
        enum ptree_type {
            ptree_type_unknown,
            ptree_type_info,
            ptree_type_json,
            ptree_type_xml,
            ptree_type_ini
        };

        asset_ptree (boost::property_tree::ptree, ptree_type = ptree_type_unknown);
        const boost::property_tree::ptree& ptree () const;
        ptree_type ptree_type_hint () const;
    private:
        boost::property_tree::ptree m_ptree;
        ptree_type m_ptree_type;
    };

    typedef std::shared_ptr<asset_ptree> asset_ptree_shared;
    typedef std::unique_ptr<asset_ptree> asset_ptree_unique;

}
