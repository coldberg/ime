#pragma once

#include "ime_asset_base.hpp"

namespace ime {

    struct asset_text final: 
        public asset_base_t<asset_text, asset_blob_base>
    {
        asset_text (std::unique_ptr<const char []> m_text, std::size_t m_size);

        const void* data () const override;
        std::size_t size () const override;

        const char* cstring () const;
        std::string string () const;
        std::size_t length () const;

    private:
        std::unique_ptr<const char []> m_text;
        std::size_t m_size;
    };

    typedef std::shared_ptr<asset_text> asset_text_shared;

}
