#pragma once
#include "ime_math.hpp"

#include <type_traits>

namespace ime {

    template <typename T> struct is_arithmetic:                  std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tvec2<T>>:   std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tvec3<T>>:   std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tvec4<T>>:   std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat2x2<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat2x3<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat2x4<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat3x2<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat3x3<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat3x4<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat4x2<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat4x3<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tmat4x4<T>>: std::is_arithmetic<T> {};
    template <typename T> struct is_arithmetic<ime::tquat<T>>:   std::is_arithmetic<T> {};

    template <typename T>
    struct is_container {

    private:
        template <typename T
                 ,typename D = decltype (std::data (std::declval<T> ()))
                 ,typename S = decltype (std::size (std::declval<T> ()))>
        static short test (const T&) 
        {}

        static  char test (...) 
        {}

    public:
        static const bool value = sizeof (test(std::declval<T> ())) != 1;

    };

}