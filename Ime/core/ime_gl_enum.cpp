#include "ime_gl_enum.hpp"
#include <GL/glew.h>

auto ime::to_gl_enum (gl_stage_enum_type s_stage) -> std::uint32_t {
    switch (s_stage) {
        case gl_enum_frag_stage: return GL_FRAGMENT_SHADER;
        case gl_enum_vert_stage: return GL_VERTEX_SHADER;
        case gl_enum_tese_stage: return GL_TESS_EVALUATION_SHADER;
        case gl_enum_tesc_stage: return GL_TESS_CONTROL_SHADER;
        case gl_enum_geom_stage: return GL_GEOMETRY_SHADER;
        case gl_enum_comp_stage: return GL_COMPUTE_SHADER;
    }
    return GL_INVALID_ENUM;
}

auto ime::to_gl_enum (gl_data_type_enum_type s_type) -> std::pair<std::uint32_t, std::uint32_t> {
    switch (s_type) {
        case ime::uint8_data_type:                  return {GL_UNSIGNED_BYTE,                GL_FALSE};
        case ime::uint16_data_type:                 return {GL_UNSIGNED_SHORT,               GL_FALSE};
        case ime::uint32_data_type:                 return {GL_UNSIGNED_INT,                 GL_FALSE};
        case ime::uint64_data_type:                 return {GL_UNSIGNED_INT64_NV,            GL_FALSE};
        case ime::int8_data_type:                   return {GL_BYTE,                         GL_FALSE};
        case ime::int16_data_type:                  return {GL_SHORT,                        GL_FALSE};
        case ime::int32_data_type:                  return {GL_INT,                          GL_FALSE};
        case ime::int64_data_type:                  return {GL_INT64_NV,                     GL_FALSE};
        case ime::uint8_data_type_norm:             return {GL_UNSIGNED_BYTE,                GL_TRUE};
        case ime::uint16_data_type_norm:            return {GL_UNSIGNED_SHORT,               GL_TRUE};
        case ime::uint32_data_type_norm:            return {GL_UNSIGNED_INT,                 GL_TRUE};
        case ime::uint64_data_type_norm:            return {GL_UNSIGNED_INT64_NV,            GL_TRUE};
        case ime::int8_data_type_norm:              return {GL_BYTE,                         GL_TRUE};
        case ime::int16_data_type_norm:             return {GL_SHORT,                        GL_TRUE};
        case ime::int32_data_type_norm:             return {GL_INT,                          GL_TRUE};
        case ime::int64_data_type_norm:             return {GL_INT64_NV,                     GL_TRUE};
        case ime::float16_data_type:                return {GL_HALF_FLOAT,                   GL_FALSE};
        case ime::float32_data_type:                return {GL_FLOAT,                        GL_FALSE};
        case ime::float64_data_type:                return {GL_DOUBLE,                       GL_FALSE};
        case ime::fixed_16_16_data_type:            return {GL_FIXED,                        GL_FALSE};
        case ime::packed_int_2_10_10_10_data_type:  return {GL_INT_2_10_10_10_REV,           GL_FALSE};
        case ime::packed_uint_2_10_10_10_data_type: return {GL_UNSIGNED_INT_2_10_10_10_REV,  GL_FALSE};
        case ime::packed_uint_10_11_11_data_type:   return {GL_UNSIGNED_INT_10F_11F_11F_REV, GL_FALSE};
    }
    return {GL_INVALID_ENUM, GL_INVALID_ENUM};
}


namespace ime {
    namespace detail {

        void gl_enum_cast_imp (gl_interpolation_mode_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_interpolation_mode_enum_type> s_key_to_val = {
                {"nearest", gl_enum_nearest},
                {"linear", gl_enum_linear},
                {"linear_mipmap_linear", gl_enum_linear_mipmap_linear},
                {"linear_mipmap_nearest", gl_enum_linear_mipmap_nearest},
                {"nearest_mipmap_linear", gl_enum_nearest_mipmap_linear},
                {"nearest_mipmap_nearest", gl_enum_nearest_mipmap_nearest}
            };
            s_value = s_key_to_val.at (s_key);
        }

        void gl_enum_cast_imp (gl_blend_func_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_blend_func_enum_type> s_key_to_val = {
                {"zero", gl_blend_func_zero},
                {"one", gl_blend_func_one},
                {"src_color", gl_blend_func_src_color},
                {"one_minus_src_color", gl_blend_func_one_minus_src_color},
                {"dst_color", gl_blend_func_dst_color},
                {"one_minus_dst_color", gl_blend_func_one_minus_dst_color},
                {"src_alpha", gl_blend_func_src_alpha},
                {"one_minus_src_alpha", gl_blend_func_one_minus_src_alpha},
                {"dst_alpha", gl_blend_func_dst_alpha},
                {"one_minus_dst_alpha", gl_blend_func_one_minus_dst_alpha},
                {"constant_color", gl_blend_func_constant_color},
                {"one_minus_constant_color", gl_blend_func_one_minus_constant_color},
                {"constant_alpha", gl_blend_func_constant_alpha},
                {"one_minus_constant_alpha", gl_blend_func_one_minus_constant_alpha},
                {"src_alpha_saturate", gl_blend_func_src_alpha_saturate},
                {"src1_color", gl_blend_func_src1_color},
                {"one_minus_src1_color", gl_blend_func_one_minus_src1_color},
                {"src1_alpha", gl_blend_func_src1_alpha},
                {"one_minus_src1_alpha", gl_blend_func_one_minus_src1_alpha}
            };
            s_value = s_key_to_val.at (s_key);
        }

        void gl_enum_cast_imp (gl_blend_equation_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_blend_equation_enum_type> s_key_to_val = {
                {"add", gl_blend_equation_add},
                {"subtract", gl_blend_equation_subtract},
                {"reverse_subtract", gl_blend_equation_reverse_subtract},
                {"min", gl_blend_equation_min},
                {"max", gl_blend_equation_max}
            };
            s_value = s_key_to_val.at (s_key);
        }

        void gl_enum_cast_imp (gl_texture_parameter_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_texture_parameter_enum_type> s_key_to_val = {
                {"depth_stencil_mode", gl_texture_parameter_depth_stencil_mode},
                {"base_level", gl_texture_parameter_base_level},
                {"compare_func", gl_texture_parameter_compare_func},
                {"compare_mode", gl_texture_parameter_compare_mode},
                {"lod_bias", gl_texture_parameter_lod_bias},
                {"min_filter", gl_texture_parameter_min_filter},
                {"mag_filter", gl_texture_parameter_mag_filter},
                {"min_lod", gl_texture_parameter_min_lod},
                {"max_lod", gl_texture_parameter_max_lod},
                {"max_level", gl_texture_parameter_max_level},
                {"swizzle_r", gl_texture_parameter_swizzle_r},
                {"swizzle_g", gl_texture_parameter_swizzle_g},
                {"swizzle_b", gl_texture_parameter_swizzle_b},
                {"swizzle_a", gl_texture_parameter_swizzle_a},
                {"wrap_s", gl_texture_parameter_wrap_s},
                {"wrap_t", gl_texture_parameter_wrap_t},
                {"wrap_r", gl_texture_parameter_wrap_r}
            };
            s_value = s_key_to_val.at (s_key);
        }

        void gl_enum_cast_imp (gl_texel_format_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_texel_format_enum_type> s_key_to_val = {
                {"r8", gl_texel_format_r8},
                {"r8_snorm", gl_texel_format_r8_snorm},
                {"r16", gl_texel_format_r16},
                {"r16_snorm", gl_texel_format_r16_snorm},
                {"rg8", gl_texel_format_rg8},
                {"rg8_snorm", gl_texel_format_rg8_snorm},
                {"rg16", gl_texel_format_rg16},
                {"rg16_snorm", gl_texel_format_rg16_snorm},
                {"r3_g3_b2", gl_texel_format_r3_g3_b2},
                {"rgb4", gl_texel_format_rgb4},
                {"rgb5", gl_texel_format_rgb5},
                {"rgb8", gl_texel_format_rgb8},
                {"rgb8_snorm", gl_texel_format_rgb8_snorm},
                {"rgb10", gl_texel_format_rgb10},
                {"rgb12", gl_texel_format_rgb12},
                {"rgb16_snorm", gl_texel_format_rgb16_snorm},
                {"rgba2", gl_texel_format_rgba2},
                {"rgba4", gl_texel_format_rgba4},
                {"rgb5_a1", gl_texel_format_rgb5_a1},
                {"rgba8", gl_texel_format_rgba8},
                {"rgba8_snorm", gl_texel_format_rgba8_snorm},
                {"rgb10_a2", gl_texel_format_rgb10_a2},
                {"rgb10_a2ui", gl_texel_format_rgb10_a2ui},
                {"rgba12", gl_texel_format_rgba12},
                {"rgba16", gl_texel_format_rgba16},
                {"srgb8", gl_texel_format_srgb8},
                {"srgb8_alpha8", gl_texel_format_srgb8_alpha8},
                {"r16f", gl_texel_format_r16f},
                {"rg16f", gl_texel_format_rg16f},
                {"rgb16f", gl_texel_format_rgb16f},
                {"rgba16f", gl_texel_format_rgba16f},
                {"r32f", gl_texel_format_r32f},
                {"rg32f", gl_texel_format_rg32f},
                {"rgb32f", gl_texel_format_rgb32f},
                {"rgba32f", gl_texel_format_rgba32f},
                {"r11f_g11f_b10f", gl_texel_format_r11f_g11f_b10f},
                {"rgb9_e5", gl_texel_format_rgb9_e5},
                {"r8i", gl_texel_format_r8i},
                {"r8ui", gl_texel_format_r8ui},
                {"r16i", gl_texel_format_r16i},
                {"r16ui", gl_texel_format_r16ui},
                {"r32i", gl_texel_format_r32i},
                {"r32ui", gl_texel_format_r32ui},
                {"rg8i", gl_texel_format_rg8i},
                {"rg8ui", gl_texel_format_rg8ui},
                {"rg16i", gl_texel_format_rg16i},
                {"rg16ui", gl_texel_format_rg16ui},
                {"rg32i", gl_texel_format_rg32i},
                {"rg32ui", gl_texel_format_rg32ui},
                {"rgb8i", gl_texel_format_rgb8i},
                {"rgb8ui", gl_texel_format_rgb8ui},
                {"rgb16i", gl_texel_format_rgb16i},
                {"rgb16ui", gl_texel_format_rgb16ui},
                {"rgb32i", gl_texel_format_rgb32i},
                {"rgb32ui", gl_texel_format_rgb32ui},
                {"rgba8i", gl_texel_format_rgba8i},
                {"rgba8ui", gl_texel_format_rgba8ui},
                {"rgba16i", gl_texel_format_rgba16i},
                {"rgba16ui", gl_texel_format_rgba16ui},
                {"rgba32i", gl_texel_format_rgba32i},
                {"rgba32ui", gl_texel_format_rgba32ui},
            };
            s_value = s_key_to_val.at (s_key);
        }

        void gl_enum_cast_imp (gl_texture_target_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_texture_target_enum_type> s_key_to_val = {
                {"texture1d", gl_enum_texture1d},
                {"texture1d_array", gl_enum_texture1d_array},
                {"texture2d", gl_enum_texture2d},
                {"texture2d_array", gl_enum_texture2d_array},
                {"texture2d_cube", gl_enum_texture2d_cube},
                {"texture2d_cube_array", gl_enum_texture2d_cube_array},
                {"texture2d_multisample", gl_enum_texture2d_multisample},
                {"texture2d_multisample_array", gl_enum_texture2d_multisample_array},
                {"texture2d_rectangle", gl_enum_texture2d_rectangle},
                {"texture3d", gl_enum_texture3d},
                {"texture1d_buffer", gl_enum_texture1d_buffer},
            };
            s_value = s_key_to_val.at (s_key);
        };

        void gl_enum_cast_imp (gl_texture_wrap_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_texture_wrap_enum_type> s_key_to_val = {
                {"repeat", gl_enum_repeat},
                {"clamp_to_edge", gl_enum_clamp_to_edge},
                {"clamp_to_border", gl_enum_clamp_to_border},
                {"mirrored_repeat", gl_enum_mirrored_repeat},
                {"mirror_clamp_to_edge", gl_enum_mirror_clamp_to_edge}
            };
            s_value = s_key_to_val.at (s_key);
        };

        void gl_enum_cast_imp (gl_buffer_access_flags_enum_type& s_value, const std::string& s_key) {
            static const std::unordered_map<std::string, gl_buffer_access_flags_enum_type> s_key_to_val = {};
            s_value = s_key_to_val.at (s_key);
        };
    }
}