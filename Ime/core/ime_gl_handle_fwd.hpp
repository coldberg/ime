#pragma once

#include <cstdint>
#include "ime_gl_context_fwd.hpp"

namespace ime {

    struct gl_texture_handle_tag {};
    struct gl_buffer_handle_tag {};
    struct gl_vertex_array_handle_tag {};
    struct gl_shader_handle_tag {};
    struct gl_program_handle_tag {};

    template <typename _Handle, typename _Tag>
    struct gl_handle_non_copyable {
        typedef _Handle value_type;

        gl_handle_non_copyable (const gl_handle_non_copyable& s_prev) = delete;
        gl_handle_non_copyable& operator = (const gl_handle_non_copyable&) = delete;
        gl_handle_non_copyable (gl_handle_non_copyable&& s_prev);
        gl_handle_non_copyable& operator = (gl_handle_non_copyable&& s_prev);
        gl_handle_non_copyable (const value_type& s_value = value_type (), gl_context_weak s_api = gl_context_weak());
        ~gl_handle_non_copyable ();
        value_type value () const;
        const ime::gl_context_weak& api () const;
    private:
        value_type m_value;
        ime::gl_context_weak m_api;
    };

    typedef gl_handle_non_copyable<std::uint32_t, gl_shader_handle_tag> gl_shader_handle;
    typedef gl_handle_non_copyable<std::uint32_t, gl_program_handle_tag> gl_program_handle;
    typedef gl_handle_non_copyable<std::uint32_t, gl_buffer_handle_tag> gl_buffer_handle;
    typedef gl_handle_non_copyable<std::uint32_t, gl_texture_handle_tag> gl_texture_handle;
    typedef gl_handle_non_copyable<std::uint32_t, gl_vertex_array_handle_tag> gl_vertex_array_handle;

};