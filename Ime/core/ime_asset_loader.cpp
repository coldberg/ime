#include "ime_asset_loader.hpp"
#include "ime_asset_type_blob.hpp"
#include "ime_utils.hpp"
#include "ime_log.hpp"

#include <unordered_map>
#include <utility>

static std::unordered_map<std::string, ime::asset_loader::loader_locate_function> st_loader_registry;

ime::asset_loader& ime::asset_loader::locate (const std::string& s_mime_type) {
    auto& s_loader_slot = st_loader_registry [s_mime_type];
    if (s_loader_slot != nullptr) {
        return s_loader_slot (s_mime_type);
    }
    throw std::runtime_error ("Can't locate loader for type : " + s_mime_type + ".");
}

ime::asset_loader::loader_locate_function 
    ime::asset_loader::register_loader (const std::string& s_mime_type, loader_locate_function s_create_function) 
{
    return std::exchange (st_loader_registry [s_mime_type], std::move (s_create_function));
}

ime::asset_loader::loader_locate_function ime::asset_loader::unregister_loader (const std::string& s_mime_type) {
    auto it = st_loader_registry.find (s_mime_type);
    if (it != st_loader_registry.end ()) {
        auto s_old_callback = std::move (it->second);
        st_loader_registry.erase (it);
        return s_old_callback;
    }
    return nullptr;
}

static struct asset_loader_octet_stream: 
    public ime::asset_loader 
{
    ime::asset_shared load (const std::string& s_key, const ime::asset_locator& s_locator) const override {
        auto s_file = s_locator.access ();        
        if (s_file->is_good ()) {
            auto s_size = s_file->size ();
            auto s_data = s_file->map_read_only (0u, s_size);
            if (!s_file->is_good ()) {
                ime::log.warn (__FILE__ ": unable to access %s.", s_locator.location ());
                return nullptr;
            }
            auto s_copy = ime::duplicate_memory ((const std::uint8_t *)s_data, s_size);
            return ime::asset_blob::new_shared (std::move (s_copy), s_size);
        }
        return nullptr;
    }

    asset_loader_octet_stream () {
        ime::asset_loader::register_loader (
        "application/octet-stream", 
        [this] (const std::string& s_mime_type) 
            -> ime::asset_loader& 
        {
            return *this;
        });
    }

} st_octet_loader;