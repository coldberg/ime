#pragma once

#include <cstddef>
#include <cstdint>
#include <algorithm>

namespace ime {

    struct image_format_descriptor {
        constexpr image_format_descriptor (
            std::uint8_t s_shift_red,
            std::uint8_t s_size_red,
            std::uint8_t s_shift_green,
            std::uint8_t s_size_green,
            std::uint8_t s_shift_blue,
            std::uint8_t s_size_blue,
            std::uint8_t s_shift_alpha,                                   
            std::uint8_t s_size_alpha)
        :   m_red_shift   (s_shift_red  ),
            m_red_size    (s_size_red   ),
            m_green_shift (s_shift_green),
            m_green_size  (s_size_green ),
            m_blue_shift  (s_shift_blue ),
            m_blue_size   (s_size_blue  ),
            m_alpha_shift (s_shift_alpha),
            m_alpha_size  (s_size_alpha )                        
        {}
    

        constexpr auto size_in_bits () const {
            return std::max (std::max (m_red_size   ? m_red_shift   + m_red_size   : 0,
                                       m_green_size ? m_green_shift + m_green_size : 0),
                             std::max (m_blue_size  ? m_blue_shift  + m_blue_size  : 0,
                                       m_alpha_size ? m_alpha_shift + m_alpha_size : 0));
        }

        constexpr auto size_in_bytes () const { return (size_in_bits () + 7) / 8; }

        constexpr auto red_shift () const { return m_red_shift; }
        constexpr auto red_size () const { return m_red_size; }

        constexpr auto green_shift () const { return m_green_shift; }
        constexpr auto green_size () const { return m_green_size; }

        constexpr auto blue_shift () const { return m_blue_shift; }
        constexpr auto blue_size () const { return m_blue_size; }

        constexpr auto alpha_shift () const { return m_alpha_shift; }
        constexpr auto alpha_size () const { return m_alpha_size; }

        template <typename T = std::uint64_t> constexpr auto red_mask () const { return ((T (1) << red_size ()) - 1u) << red_shift (); }
        template <typename T = std::uint64_t> constexpr auto green_mask () const { return ((T (1) << green_size ()) - 1u) << green_shift (); }
        template <typename T = std::uint64_t> constexpr auto blue_mask () const { return ((T (1) << blue_size ()) - 1u) << blue_shift (); }
        template <typename T = std::uint64_t> constexpr auto alpha_mask () const { return ((T (1) << alpha_size ()) - 1u) << alpha_shift (); }

        

    private:
        std::uint8_t m_red_shift;
        std::uint8_t m_red_size;
        std::uint8_t m_green_shift;
        std::uint8_t m_green_size;
        std::uint8_t m_blue_shift;
        std::uint8_t m_blue_size;
        std::uint8_t m_alpha_shift;
        std::uint8_t m_alpha_size;
    };

    inline bool operator < (const image_format_descriptor& a, 
                            const image_format_descriptor& b) 
    {
        if (a.red_mask () != b.red_mask ()) {
            return a.red_mask () < b.red_mask ();
        }
        if (a.green_mask () != b.green_mask ()) {
            return a.green_mask () < b.green_mask ();
        }
        if (a.blue_mask () != b.blue_mask ()) {
            return a.blue_mask () < b.blue_mask ();
        }
        if (a.alpha_mask () != b.alpha_mask ()) {
            return a.alpha_mask () < b.alpha_mask ();
        }
        return false;
    }

    inline bool operator > (const image_format_descriptor& a,
                            const image_format_descriptor& b) 
    {
        return b < a;
    }

    inline bool operator == (const image_format_descriptor& a,
                             const image_format_descriptor& b) 
    {
        return !(a < b) && !(a > b);
    }

    inline bool operator != (const image_format_descriptor& a,
                             const image_format_descriptor& b) 
    {
        return !(a == b);
    }

    inline bool operator <= (const image_format_descriptor& a,
                             const image_format_descriptor& b) 
    {
        return (a < b) || (a == b);
    }

    inline bool operator >= (const image_format_descriptor& a,
                             const image_format_descriptor& b) 
    {
        return (a > b) || (a == b);
    }


    static constexpr image_format_descriptor image_format_argb16161616 {32, 16, 16, 16,  0, 16, 48, 16};
    static constexpr image_format_descriptor image_format_abgr16161616 { 0, 16, 16, 16, 32, 16, 48, 16};
    static constexpr image_format_descriptor image_format_rgba16161616 {48, 16, 32, 16, 16, 16,  0, 16};
    static constexpr image_format_descriptor image_format_bgra16161616 {16, 16, 32, 16, 48, 16,  0, 16};
    static constexpr image_format_descriptor image_format_rgb161616    {32, 16, 16, 16,  0, 16,  0,  0};
    static constexpr image_format_descriptor image_format_bgr161616    { 0, 16, 16, 16, 32, 16,  0,  0};
    static constexpr image_format_descriptor image_format_ga1616       {16, 16,  0,  0,  0,  0,  0, 16};
    static constexpr image_format_descriptor image_format_ag1616       { 0, 16,  0,  0,  0,  0, 16, 16};
    static constexpr image_format_descriptor image_format_g16          { 0, 16,  0,  0,  0,  0,  0,  0};

    static constexpr image_format_descriptor image_format_argb8888     {16, 8,  8, 8,  0, 8, 24, 8};
    static constexpr image_format_descriptor image_format_abgr8888     { 0, 8,  8, 8, 16, 8, 24, 8};
    static constexpr image_format_descriptor image_format_rgba8888     {24, 8, 16, 8,  8, 8,  0, 8};
    static constexpr image_format_descriptor image_format_bgra8888     { 8, 8, 16, 8, 24, 8,  0, 8};
    static constexpr image_format_descriptor image_format_rgb888       {16, 8,  8, 8,  0, 8,  0, 0};
    static constexpr image_format_descriptor image_format_bgr888       { 0, 8,  8, 8, 16, 8,  0, 0};
    static constexpr image_format_descriptor image_format_ga88         { 8, 8,  0, 0,  0, 0,  0, 8};
    static constexpr image_format_descriptor image_format_ag88         { 0, 8,  0, 0,  0, 0,  8, 8};
    static constexpr image_format_descriptor image_format_g8           { 0, 8,  0, 0,  0, 0,  0, 0};
                                                                       
    static constexpr image_format_descriptor image_format_argb1555     {10, 5,  5, 5,  0, 5, 15, 1};
    static constexpr image_format_descriptor image_format_abgr1555     { 0, 5,  5, 5, 10, 5, 15, 1};
    static constexpr image_format_descriptor image_format_bgr555       { 0, 5,  5, 5, 10, 5,  0, 0};
    static constexpr image_format_descriptor image_format_rgb555       {10, 5,  5, 5,  0, 5,  0, 0};
    static constexpr image_format_descriptor image_format_rgb565       {11, 5,  5, 6,  0, 5,  0, 0};
    static constexpr image_format_descriptor image_format_bgr565       { 0, 5,  5, 6, 11, 5,  0, 0};


    template <typename _Itype, typename _Otype = _Itype>
    inline _Otype convert_texel_value (const image_format_descriptor& s_dst, const image_format_descriptor& s_src, const _Itype& s_value) {
        static const auto clamp = [] (auto v, auto n, auto x) {
            typedef decltype (v+n+x) value_type;
            if ((value_type)v < (value_type)n) {
                return (value_type)n;
            }
            if ((value_type)v > (value_type)x) {
                return (value_type)x;
            }
            return (value_type)v;
        };

        static const auto fn_convert_channel = [] (
            std::uint8_t s_src_shift, std::uint8_t s_src_size,
            std::uint8_t s_dst_shift, std::uint8_t s_dst_size,
            const _Itype& s_value) 
        {
            auto s_size_diff = s_dst_size - s_src_size;

            const auto s_src_mask = ((_Itype (1) << s_src_size) - _Itype (1));
            const auto s_dst_mask = ((_Otype (1) << s_dst_size) - _Otype (1));

            auto s_src_cval = (s_value >> s_src_shift) & s_src_mask;            

            if (s_size_diff >= 0) {
                //const auto s_lsb = ((_Otype (1) << +s_size_diff) - _Otype (1));
                return clamp ((((s_src_cval << +s_size_diff)/* | s_lsb*/)), _Otype (0), s_dst_mask) << s_dst_shift;
            } else {
                const auto s_lsb = ((_Otype (1) << -s_size_diff) >> _Otype (1));
                return clamp ((((s_src_cval + s_lsb) >> -s_size_diff)), _Otype (0), s_dst_mask) << s_dst_shift;
            }
            return _Otype ();
        };

        return fn_convert_channel (s_src.red_shift (),   s_src.red_size (),   s_dst.red_shift (),   s_dst.red_size (),   s_value)
             | fn_convert_channel (s_src.green_shift (), s_src.green_size (), s_dst.green_shift (), s_dst.green_size (), s_value)
             | fn_convert_channel (s_src.blue_shift (),  s_src.blue_size (),  s_dst.blue_shift (),  s_dst.blue_size (),  s_value)
             | fn_convert_channel (s_src.alpha_shift (), s_src.alpha_size (), s_dst.alpha_shift (), s_dst.alpha_size (), s_value);

    }

}

#include <functional>
namespace std {
    template <>
    struct hash<ime::image_format_descriptor>        
    {
        std::size_t operator () (const ime::image_format_descriptor& s_ifd) const {
            typedef std::uint64_t value_type;
            static_assert (sizeof (value_type) == sizeof (s_ifd), "Image descriptor of wrong size");
            const static hash<value_type> s_hash;
            return s_hash (reinterpret_cast<const value_type&> (s_ifd));
        }
    };
}