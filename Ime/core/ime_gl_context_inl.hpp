#pragma once

#include "ime_gl_context.hpp"
#include "ime_gl_asset_shader.hpp"

namespace ime {

    template <typename _Container, typename _Value>
    inline gl_buffer_handle gl_context::buffer_create (const _Container& s_view, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        return buffer_create (std::data (s_view), std::size (s_view) * sizeof (_Value), s_access);
    }

    template <typename _Container, typename _Value>
    inline void gl_context::buffer_update (gl_buffer_handle& s_buffer, const _Container& s_view, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        return buffer_update (s_buffer, std::data (s_view), std::size (s_view) * sizeof (_Value), s_access);
    }

    template <typename T>
    ime::array_view<T> gl_context::buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access, bool s_align) const {
        auto* s_ptr = (T*)buffer_map (s_buffer, (s_align ? sizeof (T) : 1u) * s_offset, s_size * sizeof (T), s_access);
        return ime::array_view<T> {s_ptr, s_ptr + s_size};
    }

    template <typename T>
    ime::array_view<T> gl_context::buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        auto* s_ptr = (T*)buffer_map (s_buffer, s_size * sizeof (T), s_access);
        return ime::array_view<T> {s_ptr, s_ptr + s_size};
    }

    template <typename T>
    ime::array_view<T> gl_context::buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        std::size_t s_size = buffer_size (s_buffer);
        auto* s_ptr = (T*)buffer_map (s_buffer, s_size, s_access);
        return ime::array_view<T> {s_ptr, s_ptr + s_size/sizeof (T)};
    }

    inline void ime::gl_context::buffer_flush (const gl_buffer_handle& s_buffer, std::size_t s_size) const {
        buffer_flush (s_buffer, 0u, s_size);
    }

    inline void ime::gl_context::buffer_flush (const gl_buffer_handle& s_buffer) const {
        buffer_flush (s_buffer, 0u, buffer_size (s_buffer));
    }

    inline void* ime::gl_context::buffer_map (const gl_buffer_handle & s_buffer, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        return buffer_map (s_buffer, 0u, s_size, s_access);
    }

    inline void* ime::gl_context::buffer_map (const gl_buffer_handle & s_buffer, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
        return buffer_map (s_buffer, 0u, buffer_size (s_buffer), s_access);
    }

    /////////////////////////

    template <typename _Value>
    inline void gl_context::uniform_array_internal (const gl_program_handle& s_prg, const char* s_loc, std::size_t s_count, const _Value* s_data) const {
        return uniform_array_internal (s_prg, uniform_location (s_prg, s_loc), s_count, s_data);
    }

    template <typename _Value>
    inline void gl_context::uniform_array_internal (const gl_program_handle& s_prg, const std::string& s_loc, std::size_t s_count, const _Value* s_data) const {
        return uniform_array_internal (s_prg, uniform_location (s_prg, s_loc.c_str ()), s_count, s_data);
    }

    //////////////////////////

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_program_handle& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const {
        return uniform_array_internal (s_prg, s_loc, s_count, s_data);
    }

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_program_handle& s_prg, const _Name& s_loc, const _Value& s_data) const {
        return uniform_array_internal (s_prg, s_loc, 1u, &s_data);
    }

    template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int>>
    void gl_context::uniform (const gl_program_handle& s_prg, const _Name& s_loc, const _Value& s_data) const {
        return uniform_array_internal (s_prg, s_loc, std::size (s_data), std::data (s_data));
    }

    //////////////////////////

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_shader_pass& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const {
        return uniform_array_internal (s_prg.handle (), s_loc, s_count, s_data);
    }

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_shader_pass& s_prg, const _Name& s_loc, const _Value& s_data) const {
        return uniform_array_internal (s_prg.handle (), s_loc, 1u, &s_data);
    }

    template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int>>
    void gl_context::uniform (const gl_shader_pass& s_prg, const _Name& s_loc, const _Value& s_data) const {
        return uniform_array_internal (s_prg.handle (), s_loc, std::size (s_data), std::data (s_data));
    }

    //////////////////////////

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_asset_shader& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const {
        for (const auto& s_pass: s_prg.passes ()) uniform_array_internal (s_pass.handle (), s_loc, s_count, s_data);
    }

    template<typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int>>
    inline void gl_context::uniform (const gl_asset_shader& s_prg, const _Name& s_loc, const _Value& s_data) const {
        for (const auto& s_pass: s_prg.passes ()) uniform_array_internal (s_pass.handle (), s_loc, 1u, &s_data);
    }

    template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int>>
    void gl_context::uniform (const gl_asset_shader& s_prg, const _Name& s_loc, const _Value& s_data) const {
        for (const auto& s_pass: s_prg.passes ()) uniform_array_internal (s_pass.handle (), s_loc, std::size (s_data), std::data (s_data));
    }

}