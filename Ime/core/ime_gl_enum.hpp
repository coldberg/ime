#pragma once

#include <cstdint>
#include <tuple>
#include <string>
#include <unordered_map>

namespace gl_ns__ {
    #include <GL/glew.h>
}

namespace ime {

    enum gl_buffer_enum_type: std::uint32_t {
        gl_enum_array_buffer                = GL_ARRAY_BUFFER                ,
        gl_enum_atomic_counter_buffer       = GL_ATOMIC_COUNTER_BUFFER       ,
        gl_enum_copy_read_buffer            = GL_COPY_READ_BUFFER            ,
        gl_enum_copy_write_buffer           = GL_COPY_WRITE_BUFFER           ,
        gl_enum_dispatch_indirect_buffer    = GL_DISPATCH_INDIRECT_BUFFER    ,
        gl_enum_draw_indirect_buffer        = GL_DRAW_INDIRECT_BUFFER        ,
        gl_enum_element_array_buffer        = GL_ELEMENT_ARRAY_BUFFER        ,
        gl_enum_pixel_pack_buffer           = GL_PIXEL_PACK_BUFFER           ,
        gl_enum_pixel_unpack_buffer         = GL_PIXEL_UNPACK_BUFFER         ,
        gl_enum_query_buffer                = GL_QUERY_BUFFER                ,
        gl_enum_shader_storage_buffer       = GL_SHADER_STORAGE_BUFFER       ,
        gl_enum_texture_buffer              = GL_TEXTURE_BUFFER              ,
        gl_enum_transform_feedback_buffer   = GL_TRANSFORM_FEEDBACK_BUFFER   ,
        gl_enum_uniform_buffer              = GL_UNIFORM_BUFFER              
    };

    enum gl_buffer_usage_hint_enum_type: std::uint32_t {
        gl_enum_stream_draw                 = GL_STREAM_DRAW  ,
        gl_enum_stream_copy                 = GL_STREAM_COPY  ,
        gl_enum_stream_read                 = GL_STREAM_READ  ,
        gl_enum_static_draw                 = GL_STATIC_DRAW  ,
        gl_enum_static_copy                 = GL_STATIC_COPY  ,
        gl_enum_static_read                 = GL_STATIC_READ  ,
        gl_enum_dynamic_draw                = GL_DYNAMIC_DRAW ,
        gl_enum_dynamic_copy                = GL_DYNAMIC_COPY ,
        gl_enum_dynamic_read                = GL_DYNAMIC_READ 
    };

    enum gl_stage_enum_type: std::uint32_t {
        gl_enum_vert_stage = 0,
        gl_enum_tesc_stage = 1,
        gl_enum_tese_stage = 2,
        gl_enum_geom_stage = 3,
        gl_enum_frag_stage = 4,
        gl_enum_comp_stage = 5
    };

    enum gl_data_type_enum_type: std::uint32_t {
        uint8_data_type,
        uint16_data_type,
        uint32_data_type,
        uint64_data_type,

        int8_data_type,
        int16_data_type,
        int32_data_type,
        int64_data_type,

        uint8_data_type_norm,
        uint16_data_type_norm,
        uint32_data_type_norm,
        uint64_data_type_norm,

        int8_data_type_norm,
        int16_data_type_norm,
        int32_data_type_norm,
        int64_data_type_norm,

        float16_data_type,
        float32_data_type,
        float64_data_type,

        fixed_16_16_data_type,

        packed_int_2_10_10_10_data_type,
        packed_uint_2_10_10_10_data_type,
        packed_uint_10_11_11_data_type
    };

    enum gl_draw_mode_enum_type: std::uint32_t {
        gl_enum_points                              = GL_POINTS                   ,
        gl_enum_line_strip                          = GL_LINE_STRIP               ,
        gl_enum_line_loop                           = GL_LINE_LOOP                ,
        gl_enum_lines                               = GL_LINES                    ,
        gl_enum_line_strip_adjacency                = GL_LINE_STRIP_ADJACENCY     ,
        gl_enum_lines_adjacency                     = GL_LINES_ADJACENCY          ,
        gl_enum_triangle_strip                      = GL_TRIANGLE_STRIP           ,
        gl_enum_triangle_fan                        = GL_TRIANGLE_FAN             ,
        gl_enum_triangles                           = GL_TRIANGLES                ,
        gl_enum_triangle_strip_adjacency            = GL_TRIANGLE_STRIP_ADJACENCY ,
        gl_enum_triangles_adjacency                 = GL_TRIANGLES_ADJACENCY      ,
        gl_enum_patches                             = GL_PATCHES
    };

    enum gl_interpolation_mode_enum_type: std::uint32_t {
        gl_enum_nearest                             = GL_NEAREST               ,
        gl_enum_linear                              = GL_LINEAR                ,
        gl_enum_linear_mipmap_linear                = GL_LINEAR_MIPMAP_LINEAR  ,
        gl_enum_linear_mipmap_nearest               = GL_LINEAR_MIPMAP_NEAREST ,
        gl_enum_nearest_mipmap_linear               = GL_NEAREST_MIPMAP_LINEAR ,
        gl_enum_nearest_mipmap_nearest              = GL_NEAREST_MIPMAP_NEAREST
    };

    enum gl_texture_wrap_enum_type: std::uint32_t{
        gl_enum_repeat                              = GL_REPEAT,
        gl_enum_clamp_to_edge                       = GL_CLAMP_TO_EDGE,
        gl_enum_clamp_to_border                     = GL_CLAMP_TO_BORDER,
        gl_enum_mirrored_repeat                     = GL_MIRRORED_REPEAT,
        gl_enum_mirror_clamp_to_edge                = GL_MIRROR_CLAMP_TO_EDGE
    };

    enum gl_texture_target_enum_type: std::uint32_t {
        gl_enum_texture1d                           = GL_TEXTURE_1D,
        gl_enum_texture1d_array                     = GL_TEXTURE_1D_ARRAY,
        gl_enum_texture2d                           = GL_TEXTURE_2D,
        gl_enum_texture2d_array                     = GL_TEXTURE_2D_ARRAY,
        gl_enum_texture2d_cube                      = GL_TEXTURE_CUBE_MAP,
        gl_enum_texture2d_cube_array                = GL_TEXTURE_CUBE_MAP_ARRAY,
        gl_enum_texture2d_multisample               = GL_TEXTURE_2D_MULTISAMPLE,
        gl_enum_texture2d_multisample_array         = GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
        gl_enum_texture2d_rectangle                 = GL_TEXTURE_RECTANGLE,
        gl_enum_texture3d                           = GL_TEXTURE_3D,
        gl_enum_texture1d_buffer                    = GL_TEXTURE_BUFFER,
    };

    enum gl_blend_func_enum_type: std::uint32_t {
        gl_blend_func_zero                          = GL_ZERO                     ,
        gl_blend_func_one                           = GL_ONE                      ,
        gl_blend_func_src_color                     = GL_SRC_COLOR                ,
        gl_blend_func_one_minus_src_color           = GL_ONE_MINUS_SRC_COLOR      ,
        gl_blend_func_dst_color                     = GL_DST_COLOR                ,
        gl_blend_func_one_minus_dst_color           = GL_ONE_MINUS_DST_COLOR      ,
        gl_blend_func_src_alpha                     = GL_SRC_ALPHA                ,
        gl_blend_func_one_minus_src_alpha           = GL_ONE_MINUS_SRC_ALPHA      ,
        gl_blend_func_dst_alpha                     = GL_DST_ALPHA                ,
        gl_blend_func_one_minus_dst_alpha           = GL_ONE_MINUS_DST_ALPHA      ,
        gl_blend_func_constant_color                = GL_CONSTANT_COLOR           ,
        gl_blend_func_one_minus_constant_color      = GL_ONE_MINUS_CONSTANT_COLOR ,
        gl_blend_func_constant_alpha                = GL_CONSTANT_ALPHA           ,
        gl_blend_func_one_minus_constant_alpha      = GL_ONE_MINUS_CONSTANT_ALPHA ,
        gl_blend_func_src_alpha_saturate            = GL_SRC_ALPHA_SATURATE       ,
        gl_blend_func_src1_color                    = GL_SRC1_COLOR               ,
        gl_blend_func_one_minus_src1_color          = GL_ONE_MINUS_SRC1_COLOR     ,
        gl_blend_func_src1_alpha                    = GL_SRC1_ALPHA               ,
        gl_blend_func_one_minus_src1_alpha          = GL_ONE_MINUS_SRC1_ALPHA     
    };

    enum gl_blend_equation_enum_type: std::uint32_t {
        gl_blend_equation_add                       = GL_FUNC_ADD,
        gl_blend_equation_subtract                  = GL_FUNC_SUBTRACT,
        gl_blend_equation_reverse_subtract          = GL_FUNC_REVERSE_SUBTRACT,
        gl_blend_equation_min                       = GL_MIN,
        gl_blend_equation_max                       = GL_MAX
    };

    enum gl_texel_format_enum_type: std::uint32_t {
        gl_texel_format_dont_care                   = GL_DONT_CARE     ,
        gl_texel_format_r8	                        = GL_R8	           ,
        gl_texel_format_r8_snorm	                = GL_R8_SNORM	   ,
        gl_texel_format_r16	                        = GL_R16	       ,
        gl_texel_format_r16_snorm	                = GL_R16_SNORM	   ,
        gl_texel_format_rg8	                        = GL_RG8	       ,
        gl_texel_format_rg8_snorm	                = GL_RG8_SNORM	   ,
        gl_texel_format_rg16	                    = GL_RG16	       ,
        gl_texel_format_rg16_snorm	                = GL_RG16_SNORM	   ,
        gl_texel_format_r3_g3_b2	                = GL_R3_G3_B2	   ,
        gl_texel_format_rgb4	                    = GL_RGB4	       ,
        gl_texel_format_rgb5	                    = GL_RGB5	       ,
        gl_texel_format_rgb8	                    = GL_RGB8	       ,
        gl_texel_format_rgb8_snorm	                = GL_RGB8_SNORM	   ,
        gl_texel_format_rgb10	                    = GL_RGB10	       ,
        gl_texel_format_rgb12	                    = GL_RGB12	       ,
        gl_texel_format_rgb16_snorm	                = GL_RGB16_SNORM   ,
        gl_texel_format_rgba2	                    = GL_RGBA2	       ,
        gl_texel_format_rgba4	                    = GL_RGBA4	       ,
        gl_texel_format_rgb5_a1	                    = GL_RGB5_A1	   ,
        gl_texel_format_rgba8	                    = GL_RGBA8	       ,
        gl_texel_format_rgba8_snorm	                = GL_RGBA8_SNORM   ,
        gl_texel_format_rgb10_a2	                = GL_RGB10_A2	   ,
        gl_texel_format_rgb10_a2ui	                = GL_RGB10_A2UI	   ,
        gl_texel_format_rgba12	                    = GL_RGBA12	       ,
        gl_texel_format_rgba16	                    = GL_RGBA16	       ,
        gl_texel_format_srgb8	                    = GL_SRGB8	       ,
        gl_texel_format_srgb8_alpha8	            = GL_SRGB8_ALPHA8  ,
        gl_texel_format_r16f	                    = GL_R16F	       ,
        gl_texel_format_rg16f	                    = GL_RG16F	       ,
        gl_texel_format_rgb16f	                    = GL_RGB16F	       ,
        gl_texel_format_rgba16f	                    = GL_RGBA16F	   ,
        gl_texel_format_r32f	                    = GL_R32F	       ,
        gl_texel_format_rg32f	                    = GL_RG32F	       ,
        gl_texel_format_rgb32f	                    = GL_RGB32F	       ,
        gl_texel_format_rgba32f	                    = GL_RGBA32F	   ,
        gl_texel_format_r11f_g11f_b10f	            = GL_R11F_G11F_B10F,
        gl_texel_format_rgb9_e5	                    = GL_RGB9_E5	   ,
        gl_texel_format_r8i	                        = GL_R8I	       ,
        gl_texel_format_r8ui	                    = GL_R8UI	       ,
        gl_texel_format_r16i	                    = GL_R16I	       ,
        gl_texel_format_r16ui	                    = GL_R16UI	       ,
        gl_texel_format_r32i	                    = GL_R32I	       ,
        gl_texel_format_r32ui	                    = GL_R32UI	       ,
        gl_texel_format_rg8i	                    = GL_RG8I	       ,
        gl_texel_format_rg8ui	                    = GL_RG8UI	       ,
        gl_texel_format_rg16i	                    = GL_RG16I	       ,
        gl_texel_format_rg16ui	                    = GL_RG16UI	       ,
        gl_texel_format_rg32i	                    = GL_RG32I	       ,
        gl_texel_format_rg32ui	                    = GL_RG32UI	       ,
        gl_texel_format_rgb8i	                    = GL_RGB8I	       ,
        gl_texel_format_rgb8ui	                    = GL_RGB8UI	       ,
        gl_texel_format_rgb16i	                    = GL_RGB16I	       ,
        gl_texel_format_rgb16ui	                    = GL_RGB16UI	   ,
        gl_texel_format_rgb32i	                    = GL_RGB32I	       ,
        gl_texel_format_rgb32ui	                    = GL_RGB32UI	   ,
        gl_texel_format_rgba8i	                    = GL_RGBA8I	       ,
        gl_texel_format_rgba8ui	                    = GL_RGBA8UI	   ,
        gl_texel_format_rgba16i	                    = GL_RGBA16I	   ,
        gl_texel_format_rgba16ui	                = GL_RGBA16UI	   ,
        gl_texel_format_rgba32i	                    = GL_RGBA32I	   ,
        gl_texel_format_rgba32ui	                = GL_RGBA32UI	   ,
    };

    enum gl_texture_parameter_enum_type: std::uint32_t {
        gl_texture_parameter_depth_stencil_mode     = GL_DEPTH_STENCIL_TEXTURE_MODE, 
        gl_texture_parameter_base_level             = GL_TEXTURE_BASE_LEVEL, 
        gl_texture_parameter_compare_func           = GL_TEXTURE_COMPARE_FUNC, 
        gl_texture_parameter_compare_mode           = GL_TEXTURE_COMPARE_MODE, 
        gl_texture_parameter_lod_bias               = GL_TEXTURE_LOD_BIAS, 
        gl_texture_parameter_min_filter             = GL_TEXTURE_MIN_FILTER, 
        gl_texture_parameter_mag_filter             = GL_TEXTURE_MAG_FILTER, 
        gl_texture_parameter_min_lod                = GL_TEXTURE_MIN_LOD, 
        gl_texture_parameter_max_lod                = GL_TEXTURE_MAX_LOD, 
        gl_texture_parameter_max_level              = GL_TEXTURE_MAX_LEVEL, 
        gl_texture_parameter_swizzle_r              = GL_TEXTURE_SWIZZLE_R, 
        gl_texture_parameter_swizzle_g              = GL_TEXTURE_SWIZZLE_G, 
        gl_texture_parameter_swizzle_b              = GL_TEXTURE_SWIZZLE_B, 
        gl_texture_parameter_swizzle_a              = GL_TEXTURE_SWIZZLE_A, 
        gl_texture_parameter_wrap_s                 = GL_TEXTURE_WRAP_S, 
        gl_texture_parameter_wrap_t                 = GL_TEXTURE_WRAP_T, 
        gl_texture_parameter_wrap_r                 = GL_TEXTURE_WRAP_R,
        gl_texture_parameter_border_color           = GL_TEXTURE_BORDER_COLOR
    };

    enum gl_clear_flags_enum_type: std::uint32_t {
        gl_clear_color_buffer_bit,
        gl_clear_depth_buffer_bit,
        gl_clear_stencil_buffer_bit
    };

    enum gl_buffer_access_flags_enum_type: std::uint32_t {
        gl_buffer_access_map_read_bit               = 0,
        gl_buffer_access_map_write_bit              = 1,
        gl_buffer_access_map_invalidate_range_bit   = 2,
        gl_buffer_access_map_invalidate_buffer_bit  = 3,
        gl_buffer_access_map_flush_explicit_bit     = 4,
        gl_buffer_access_map_unsynchronized_bit     = 5,
        gl_buffer_access_map_persistent_bit         = 6,
        gl_buffer_access_map_coherent_bit           = 7,
        gl_buffer_access_dynamic_storage_bit        = 8,
        gl_buffer_access_client_storage_bit         = 9
    };    

    namespace detail {

        void gl_enum_cast_imp (gl_interpolation_mode_enum_type&  , const std::string&);
        void gl_enum_cast_imp (gl_blend_func_enum_type&          , const std::string&);
        void gl_enum_cast_imp (gl_blend_equation_enum_type&      , const std::string&);
        void gl_enum_cast_imp (gl_texture_parameter_enum_type&   , const std::string&);
        void gl_enum_cast_imp (gl_texel_format_enum_type&        , const std::string&);
        void gl_enum_cast_imp (gl_texture_target_enum_type&      , const std::string&);
        void gl_enum_cast_imp (gl_texture_wrap_enum_type&        , const std::string&);
        void gl_enum_cast_imp (gl_buffer_access_flags_enum_type& , const std::string&);

    }

    template <typename T>
    auto gl_enum_cast (const std::string& s_key) {
        T s_value;
        detail::gl_enum_cast_imp (s_value, s_key);
        return s_value;
    }

    auto to_gl_enum (gl_stage_enum_type)->std::uint32_t;
    auto to_gl_enum (gl_data_type_enum_type)->std::pair<std::uint32_t, std::uint32_t>;
}
