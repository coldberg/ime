#include <SDL.h>
#include <GL/glew.h>
#include <assert.h>

#include <sstream>
#include <cassert>
#include <stdexcept>

#include "ime_log.hpp"
#include "ime_asset_cache.hpp"
#include "ime_gl_enum.hpp"
#include "ime_gl_handle.hpp"
#include "ime_gl_context.hpp"
#include "ime_gl_asset_texture.hpp"
#include "ime_gl_asset_shader.hpp"
#include "ime_gl_loader_texture.hpp"
#include "ime_gl_loader_shader.hpp"
#include "ime_gl_shader_pass.hpp"
#include "ime_gl_texture_format.hpp"

static SDL_Window* g_gwindow = nullptr;
static SDL_GLContext g_context = nullptr;

ime::gl_context::gl_context () {
    SDL_InitSubSystem (SDL_INIT_VIDEO);

    SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute (SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute (SDL_GL_CONTEXT_PROFILE_MASK,  SDL_GL_CONTEXT_PROFILE_CORE);
    #ifdef _DEBUG
    SDL_GL_SetAttribute (SDL_GL_CONTEXT_FLAGS,         SDL_GL_CONTEXT_DEBUG_FLAG);
    #endif
    SDL_GL_SetAttribute (SDL_GL_MULTISAMPLESAMPLES,    4);
    SDL_GL_SetAttribute (SDL_GL_MULTISAMPLEBUFFERS,    1);
    SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER,          1);
    SDL_GL_SetAttribute (SDL_GL_RED_SIZE,              8);
    SDL_GL_SetAttribute (SDL_GL_GREEN_SIZE,            8);
    SDL_GL_SetAttribute (SDL_GL_BLUE_SIZE,             8);
    SDL_GL_SetAttribute (SDL_GL_ALPHA_SIZE,            0);
    SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE,            24);
    SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE,          8);

    g_gwindow = SDL_CreateWindow ("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1920u, 1080u, SDL_WINDOW_OPENGL);
    g_context = SDL_GL_CreateContext (g_gwindow);
    if (g_gwindow == nullptr || g_context == nullptr) {
        throw std::runtime_error (__FILE__ ": unable to initialize window or context.");
    }

    glewExperimental = true;
    auto glew_err = glewInit ();
    if (glew_err != GLEW_NO_ERROR) {
        throw std::runtime_error (__FILE__ ": unable to initialize opengl.");
    }

    #ifdef _DEBUG
           
    glDebugMessageCallback ([] (auto source, auto type, auto id, auto severity, auto length, auto message, auto userparam) ->void {
        ime::log.debug (" => %s\n"
            "      id : 0x%x\n"
            "  source : 0x%x\n"
            "    type : 0x%x\n"
            "severity : 0x%x\n",
            message, id, source, type, severity);
    }, nullptr);

    glDebugMessageControl (GL_DONT_CARE, GL_DEBUG_TYPE_OTHER, GL_DONT_CARE, 0u, nullptr, GL_FALSE);

    #endif
}

ime::gl_context::~gl_context () {    
    SDL_GL_DeleteContext (g_context);
    SDL_DestroyWindow (g_gwindow);
    SDL_QuitSubSystem (SDL_INIT_VIDEO);
}

auto ime::gl_context::window (const ime::ivec2& s_size) {
    SDL_SetWindowSize (g_gwindow, s_size.x, s_size.y);
    SDL_SetWindowPosition (g_gwindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
}

auto ime::gl_context::window () const -> std::pair<ime::ivec2, ime::ivec2> {
    ime::ivec2 s_origin, s_size;
    SDL_GetWindowSize (g_gwindow, &s_size.x, &s_size.y);
    SDL_GetWindowPosition (g_gwindow, &s_origin.x, &s_origin.y);
    return {s_origin, s_size};
}

auto ime::gl_context::window (const ime::ivec2& s_origin, const ime::ivec2& s_size) {
    SDL_SetWindowSize (g_gwindow, s_size.x, s_size.y);
    SDL_SetWindowPosition (g_gwindow, s_origin.x, s_origin.y);
}

auto ime::gl_context::viewport () const -> std::pair<ime::ivec2, ime::ivec2> {
    GLint s_dims [4u];
    glGetIntegerv (GL_VIEWPORT, s_dims);
    return { 
        {s_dims [0], s_dims [1]},
        {s_dims [2], s_dims [3]}
    };
}

void ime::gl_context::viewport (const ime::ivec2& s_origin, const ime::ivec2& s_size) {
    glViewport (s_origin.x, s_origin.y, s_size.x, s_size.y);
}

auto ime::gl_context::scissor () const -> std::pair<ime::ivec2, ime::ivec2> {
    GLint s_dims [4u];
    glGetIntegerv (GL_SCISSOR_BOX, s_dims);
    return{
        {s_dims [0], s_dims [1]},
        {s_dims [2], s_dims [3]}
    };
}

void ime::gl_context::scissor (const ime::ivec2& s_origin, const ime::ivec2& s_size) {
    glScissor (s_origin.x, s_origin.y, s_size.x, s_size.y);
}

void ime::gl_context::scissor_enabled (bool s_value) {
    (s_value ? glEnable : glDisable) (GL_SCISSOR_TEST);    
}

bool ime::gl_context::scissor_enabled () const {
    auto s_param = 0;
    glGetIntegerv (GL_SCISSOR_TEST, &s_param);
    return s_param != GL_FALSE;
}

void ime::gl_context::present () {
    SDL_GL_SwapWindow (g_gwindow);
}

ime::gl_program_handle ime::gl_context::program_create () const {
    return{glCreateProgram (), lock ()};
}

ime::gl_shader_handle ime::gl_context::shader_create (gl_stage_enum_type s_stage) const {
    return{glCreateShader (ime::to_gl_enum (s_stage)), lock ()};
}

bool ime::gl_context::shader_compile (const ime::gl_shader_handle& s_object, const std::string& s_text, std::vector<std::string>& s_log) const {
    auto p_size = (GLsizei)s_text.size ();
    const auto* p_text = s_text.c_str ();

    glShaderSource (s_object.value (), 1u, &p_text, &p_size);
    glCompileShader (s_object.value ());
   
    GLint s_param = 0;
    glGetShaderiv (s_object.value (), GL_INFO_LOG_LENGTH, &s_param);
    if (s_param > 0) {
        auto s_log_data = std::make_unique<char []> (s_param + 1);
        glGetShaderInfoLog (s_object.value (), s_param + 1, &s_param, s_log_data.get ());
        std::stringstream s_log_stream (s_log_data.get ());
        std::string s_line;
        //s_log.clear ();
        while (std::getline (s_log_stream, s_line)) {
            s_log.push_back (s_line);
        }
    }

    glGetShaderiv (s_object.value (), GL_COMPILE_STATUS, &s_param);
    return s_param == GL_TRUE;
}

ime::gl_shader_handle ime::gl_context::shader_compile (gl_stage_enum_type s_type, const std::string& s_text, std::vector<std::string>& s_log) const {
    auto s_object = shader_create (s_type);
    if (shader_compile (s_object, s_text, s_log)) {
        return s_object;
    }
    return gl_shader_handle ();
}

bool ime::gl_context::program_link (const ime::gl_program_handle& s_program, const std::vector<ime::gl_shader_handle>& s_all_objects, std::vector<std::string>& s_log) const {
    for (const auto& s_object: s_all_objects) {
        glAttachShader (s_program.value (), s_object.value ());
    }
    glLinkProgram (s_program.value ());

    GLint s_param = 0;
    glGetProgramiv (s_program.value (), GL_INFO_LOG_LENGTH, &s_param); 
    if (s_param > 0) {
        auto s_log_data = std::make_unique<char []> (s_param + 1);
        glGetProgramInfoLog (s_program.value (), s_param + 1, &s_param, s_log_data.get ());
        std::stringstream s_log_stream (s_log_data.get ());
        std::string s_line;
        //s_log.clear ();
        while (std::getline (s_log_stream, s_line)) {
            s_log.push_back (s_line);
        }
    }

    glGetProgramiv (s_program.value (), GL_LINK_STATUS, &s_param);
    return s_param == GL_TRUE;
}

ime::gl_program_handle ime::gl_context::program_link (const std::vector<ime::gl_shader_handle>& s_objs, std::vector<std::string>& s_log) const {
    auto s_program = program_create ();
    if (program_link (s_program, s_objs, s_log)) {
        return s_program;
    }
    return ime::gl_program_handle ();
}

void ime::gl_context::apply (const gl_program_handle& s_program) const {
    glUseProgram (s_program.value ());
}

void ime::gl_context::apply (const gl_shader_props& s_props) const {
    (s_props.depth.test ? glEnable : glDisable) (GL_DEPTH_TEST);
    glDepthMask (s_props.depth.write ? GL_TRUE : GL_FALSE);
    (s_props.blend.enabled ? glEnable : glDisable) (GL_BLEND);
    if (s_props.blend.enabled) {
        glBlendFunc (s_props.blend.src_factor, s_props.blend.dst_factor);
        glBlendEquation (s_props.blend.equation);
    }
}

void ime::gl_context::apply (const gl_shader_pass& s_pass) const {
    apply (s_pass.props ());
    apply (s_pass.handle ());
}

ime::gl_buffer_handle ime::gl_context::buffer_create (const void* s_data, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
    auto s_handle = 0u;
    glCreateBuffers (1u, &s_handle);
    glNamedBufferStorage (s_handle, s_size, s_data, s_access.to_bits ());
    return ime::gl_buffer_handle (s_handle, lock ());
}

void ime::gl_context::buffer_update (gl_buffer_handle& s_buffer, const void* s_data, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
    auto s_ptr = glMapNamedBufferRange (s_buffer.value (), 0u, s_size, s_access.to_bits () | GL_MAP_WRITE_BIT);
    std::memcpy (s_ptr, s_data, s_size);
    glFlushMappedNamedBufferRange (s_buffer.value (), 0u, s_size);
    glUnmapNamedBuffer (s_buffer.value ());
}

void ime::gl_context::buffer_flush (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size) const {
    glFlushMappedNamedBufferRange (s_buffer.value (), s_offset, s_size);
}

void ime::gl_context::buffer_unmap (const gl_buffer_handle& s_buffer) const {
    glUnmapNamedBuffer (s_buffer.value ());
}

void* ime::gl_context::buffer_map (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const {
    return glMapNamedBufferRange (s_buffer.value (), s_offset, s_size, s_access.to_bits ());
}

std::size_t ime::gl_context::buffer_size (const gl_buffer_handle & s_buffer) const {
    auto s_size = 0;
    glGetNamedBufferParameteriv (s_buffer.value (), GL_BUFFER_SIZE, &s_size);
    return (std::size_t)s_size;
}

void ime::gl_context::release (gl_texture_handle& s_handle) const {
    auto s_value = s_handle.value ();
    glDeleteTextures (1u, &s_value);
}

void ime::gl_context::release (gl_buffer_handle& s_handle) const {
    auto s_value = s_handle.value ();
    glDeleteBuffers (1u, &s_value);
}

void ime::gl_context::release (gl_vertex_array_handle& s_handle) const {
    auto s_value = s_handle.value ();
    glDeleteVertexArrays (1u, &s_value);
}

ime::gl_vertex_array_handle ime::gl_context::vertex_array_create () {
    auto s_handle = 0u;
    glCreateVertexArrays (1u, &s_handle);
    return gl_vertex_array_handle (s_handle, lock ());
}

void ime::gl_context::bind (
    gl_vertex_array_handle& s_vao, 
    const gl_buffer_handle& s_vbo, 
    std::uint32_t s_ibind, 
    std::size_t s_stride,
    const std::vector<buffer_binding_descriptor_type>& s_alldesc, 
    std::uint32_t s_per_instance) 
    const 
{

    glVertexArrayVertexBuffer (s_vao.value (), s_ibind, s_vbo.value (), 0u, (GLsizei)s_stride);
    glVertexArrayBindingDivisor (s_vao.value (), s_ibind, s_per_instance);

    for (const auto& s_desc : s_alldesc) {

        std::uint32_t            s_index;
        std::uint32_t            s_channels;
        gl_data_type_enum_type   s_data_type;
        std::size_t              s_offset;

        std::tie (s_index, s_channels, s_data_type, s_offset) = s_desc;
        auto s_type_enum = ime::to_gl_enum (s_data_type);

        glEnableVertexArrayAttrib   (s_vao.value (), s_index);
        glVertexArrayAttribFormat   (s_vao.value (), s_index, s_channels, s_type_enum.first, s_type_enum.second, (GLuint)s_offset);
        glVertexArrayAttribBinding  (s_vao.value (), s_index, s_ibind);
    }
}

void ime::gl_context::apply (const gl_vertex_array_handle& s_vao) {
    glBindVertexArray (s_vao.value ());
}

void ime::gl_context::clear (small_set<ime::gl_clear_flags_enum_type> s_flags, const ime::vec4& s_color, float s_depth, int s_stencil) {
    if (s_flags.to_bits ()) {
        auto s_gl_flags = 0u;
        if (s_flags.contains (ime::gl_clear_color_buffer_bit)) {
            s_gl_flags |= GL_COLOR_BUFFER_BIT;
            glClearColor (s_color.r, s_color.g, s_color.b, s_color.a);
        }
        if (s_flags.contains (ime::gl_clear_depth_buffer_bit)) {
            s_gl_flags |= GL_DEPTH_BUFFER_BIT;
            glClearDepth (s_depth);
        }
        if (s_flags.contains (ime::gl_clear_stencil_buffer_bit)) {
            s_gl_flags |= GL_STENCIL_BUFFER_BIT;
            glClearStencil (s_stencil);
        }
        glClear (s_gl_flags);
    }
}

void ime::gl_context::draw_arrays (const gl_shader_pass& s_pass, const gl_vertex_array_handle& s_vao, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range) {
    apply (s_pass);
    apply (s_vao);
    draw_arrays (s_mode, s_range);
}

void ime::gl_context::draw_arrays (const gl_shader_pass& s_pass, const gl_vertex_array_handle& s_vao, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances) {
    apply (s_pass);
    apply (s_vao);
    draw_arrays (s_mode, s_range, s_instances);
}

void ime::gl_context::draw_arrays (const gl_asset_shader& s_shader, const gl_vertex_array_handle& s_vao, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range) {
    for (const auto& s_pass: s_shader.passes ()) {
        draw_arrays (s_pass, s_vao, s_mode, s_range);
    }
}
void ime::gl_context::draw_arrays (const gl_asset_shader& s_shader, const gl_vertex_array_handle& s_vao, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances) {
    for (const auto& s_pass: s_shader.passes ()) {
        draw_arrays (s_pass, s_vao, s_mode, s_range, s_instances);
    }
}

void ime::gl_context::draw_arrays (gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range) {
    glDrawArrays ((GLenum)s_mode, s_range.first, s_range.second - s_range.first);
}

void ime::gl_context::draw_arrays (gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances) {
    glDrawArraysInstanced ((GLenum)s_mode, s_range.first, s_range.second - s_range.first, s_instances);
}

void ime::gl_context::release (gl_shader_handle& s_handle) const {
    glDeleteShader (s_handle.value ());
}

void ime::gl_context::release (gl_program_handle& s_handle) const {
    glDeleteProgram (s_handle.value ());
}

void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::quat*       s_data) const { uniform_array_internal      (s_prg.value (), s_loc, (GLsizei)s_count,           (const vec4*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::iquat*      s_data) const { uniform_array_internal      (s_prg.value (), s_loc, (GLsizei)s_count,           (const ivec4*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uquat*      s_data) const { uniform_array_internal      (s_prg.value (), s_loc, (GLsizei)s_count,           (const uvec4*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dquat*      s_data) const { uniform_array_internal      (s_prg.value (), s_loc, (GLsizei)s_count,           (const dvec4*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const std::int32_t*    s_data) const { glProgramUniform1iv         (s_prg.value (), s_loc, (GLsizei)s_count,           s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const std::uint32_t*   s_data) const { glProgramUniform1uiv        (s_prg.value (), s_loc, (GLsizei)s_count,           s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const float*           s_data) const { glProgramUniform1fv         (s_prg.value (), s_loc, (GLsizei)s_count,           s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const double*          s_data) const { glProgramUniform1dv         (s_prg.value (), s_loc, (GLsizei)s_count,           s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec2*       s_data) const { glProgramUniform2fv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec3*       s_data) const { glProgramUniform3fv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec4*       s_data) const { glProgramUniform4fv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec2*      s_data) const { glProgramUniform2iv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::int32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec3*      s_data) const { glProgramUniform3iv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::int32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec4*      s_data) const { glProgramUniform4iv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::int32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec2*      s_data) const { glProgramUniform2uiv        (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::uint32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec3*      s_data) const { glProgramUniform3uiv        (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::uint32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec4*      s_data) const { glProgramUniform4uiv        (s_prg.value (), s_loc, (GLsizei)s_count,           (const std::uint32_t*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec2*      s_data) const { glProgramUniform2dv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec3*      s_data) const { glProgramUniform3dv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec4*      s_data) const { glProgramUniform4dv         (s_prg.value (), s_loc, (GLsizei)s_count,           (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x2*     s_data) const { glProgramUniformMatrix2fv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x3*     s_data) const { glProgramUniformMatrix2x3fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x4*     s_data) const { glProgramUniformMatrix2x4fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x2*     s_data) const { glProgramUniformMatrix3x2fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x3*     s_data) const { glProgramUniformMatrix3fv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x4*     s_data) const { glProgramUniformMatrix3x4fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x2*     s_data) const { glProgramUniformMatrix4x2fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x3*     s_data) const { glProgramUniformMatrix4x3fv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x4*     s_data) const { glProgramUniformMatrix4fv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const float*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x2*    s_data) const { glProgramUniformMatrix2dv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x3*    s_data) const { glProgramUniformMatrix2x3dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x4*    s_data) const { glProgramUniformMatrix2x4dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x2*    s_data) const { glProgramUniformMatrix3x2dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x3*    s_data) const { glProgramUniformMatrix3dv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x4*    s_data) const { glProgramUniformMatrix3x4dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x2*    s_data) const { glProgramUniformMatrix4x2dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x3*    s_data) const { glProgramUniformMatrix4x2dv (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }
void ime::gl_context::uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x4*    s_data) const { glProgramUniformMatrix4dv   (s_prg.value (), s_loc, (GLsizei)s_count, GL_FALSE, (const double*)s_data); }

int ime::gl_context::uniform_location (const gl_program_handle& s_prg, const char* s_name) const {
    return glGetUniformLocation (s_prg.value (), s_name);
}

int ime::gl_context::attribute_location (const gl_program_handle& s_prg, const char* s_name) const {
    return glGetAttribLocation (s_prg.value (), s_name);
}

ime::gl_texture_handle ime::gl_context::texture_create (
    gl_texture_target_enum_type s_target, 
    gl_texel_format_enum_type s_internal_format, 
    const ime::uvec3& s_extent, 
    std::uint32_t s_num_levels) 
    const 
{
    auto s_int_handle = 0u;
    glCreateTextures ((GLenum)s_target, 1u, &s_int_handle);
    if (!s_int_handle) {
        return gl_texture_handle (0u, lock ());
    }

    auto s_handle = gl_texture_handle (s_int_handle, lock ());
    
    if (s_internal_format == gl_texel_format_dont_care) {
        s_internal_format = gl_texel_format_rgba8;
    }

    auto s_max_levels = 0u;
    auto s_max_texdim = 0u;

    switch (s_target) {
        case ime::gl_enum_texture1d:
        case ime::gl_enum_texture1d_array:
            s_max_texdim = s_extent.x;
            break;

        case ime::gl_enum_texture2d:
        case ime::gl_enum_texture2d_array:
        case ime::gl_enum_texture2d_multisample:
        case ime::gl_enum_texture2d_multisample_array:
            s_max_texdim = std::max (s_extent.x, s_extent.y);
            break;

        case ime::gl_enum_texture3d:
            s_max_texdim = std::max ({s_extent.x, s_extent.y, s_extent.z});
            break;

        case ime::gl_enum_texture1d_buffer:
        case ime::gl_enum_texture2d_rectangle:
            if (s_max_levels > 0) {
                ime::log.warn (__FILE__ ": mipmapping not supported for rectangle, buffer textures.");
            }
        default:
            break;
    }

    s_max_levels = (std::uint32_t)std::ceil (std::log2 (s_max_texdim));
    s_num_levels = ime::clamp (s_num_levels, 1u, s_max_levels);

    switch (s_target) {
        case ime::gl_enum_texture1d:
            glTextureStorage1D (s_handle.value (), s_num_levels, s_internal_format, s_extent.x); 
            break;        

        case ime::gl_enum_texture1d_array:
        case ime::gl_enum_texture2d_rectangle: 
            glTextureStorage2D (s_handle.value (), s_num_levels, s_internal_format, s_extent.x, s_extent.y); 
            break;
       
        case ime::gl_enum_texture2d:
        case ime::gl_enum_texture2d_multisample: 
            glTextureStorage2D (s_handle.value (), s_num_levels, s_internal_format, s_max_texdim, s_max_texdim); 
            break;

        case ime::gl_enum_texture2d_array:        
        case ime::gl_enum_texture2d_multisample_array:
            glTextureStorage3D (s_handle.value (), s_num_levels, s_internal_format, s_max_texdim, s_max_texdim, s_extent.z);
            break;

        case ime::gl_enum_texture3d:
            glTextureStorage3D (s_handle.value (), s_num_levels, s_internal_format, s_max_texdim, s_max_texdim, s_max_texdim);
            break;

        default:
            return gl_texture_handle (0u, lock ());
    }
    return s_handle;
}

void ime::gl_context::texture_update (
    const gl_texture_handle& s_handle,
    gl_texture_target_enum_type s_target,
    const ime::uvec3& s_offset,
    const ime::uvec3& s_extent,
    const void* s_data, 
    const image_format_descriptor& s_format, 
    std::uint32_t s_mipmap_level) 
    const 
{
    const auto& s_fmtdesc = gl_texture_format_descriptor::from_image_format_descriptor (s_format);
    switch (s_target) {
        case ime::gl_enum_texture1d:
            glTextureSubImage1D (s_handle.value (), s_mipmap_level, s_offset.x, s_extent.x, s_fmtdesc.base_format (), s_fmtdesc.channel_format (), s_data);
            break;

        case ime::gl_enum_texture1d_array:
        case ime::gl_enum_texture2d_rectangle:
        case ime::gl_enum_texture2d:
        case ime::gl_enum_texture2d_multisample:
            glTextureSubImage2D (s_handle.value (), s_mipmap_level, s_offset.x, s_offset.y, s_extent.x, s_extent.y, s_fmtdesc.base_format (), s_fmtdesc.channel_format (), s_data);
            break;

        case ime::gl_enum_texture2d_array:
        case ime::gl_enum_texture2d_multisample_array:
        case ime::gl_enum_texture3d:
            glTextureSubImage3D (s_handle.value (), s_mipmap_level, s_offset.x, s_offset.y, s_offset.z, s_extent.x, s_extent.y, s_extent.z, s_fmtdesc.base_format (), s_fmtdesc.channel_format (), s_data);
            break;

        default:
            break;
    }
}

void ime::gl_context::texture_build_mipmaps (const gl_texture_handle& s_texture) const {
    glGenerateTextureMipmap (s_texture.value ());
}

void ime::gl_context::texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, float s_value)  const {
    glTextureParameterf (s_handle.value (), s_param, s_value);
}
void ime::gl_context::texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, int s_value)  const {
    glTextureParameteri (s_handle.value (), s_param, s_value);
}
void ime::gl_context::texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::vec4& s_value)  const {
    glTextureParameterfv (s_handle.value (), s_param, (const float*)&s_value);
}
void ime::gl_context::texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::ivec4& s_value, float s_normalize) const {
    (s_normalize ? glTextureParameteriv : glTextureParameterIiv) (s_handle.value (), s_param, (const std::int32_t*)&s_value);
}
void ime::gl_context::texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::uvec4& s_value) const {
    glTextureParameterIuiv (s_handle.value (), s_param, (const std::uint32_t*)&s_value);
}

ime::ivec3 ime::gl_context::texture_extent (const gl_texture_handle& s_handle, std::uint32_t s_level) const {
    ivec3 s_extent;
    glGetTextureLevelParameteriv (s_handle.value (), s_level, GL_TEXTURE_WIDTH, &s_extent.x);
    glGetTextureLevelParameteriv (s_handle.value (), s_level, GL_TEXTURE_HEIGHT, &s_extent.y);
    glGetTextureLevelParameteriv (s_handle.value (), s_level, GL_TEXTURE_DEPTH, &s_extent.z);
    return s_extent;
}

void ime::gl_context::apply (const gl_texture_handle& s_tex, gl_texture_target_enum_type s_target, std::uint32_t s_slot) {    
    glActiveTexture (GL_TEXTURE0 + s_slot);
    //glEnable ((GLenum)s_target);
    glBindTexture ((GLenum)s_target, s_tex.value ());
}

void ime::gl_context::apply (const gl_asset_texture& s_tex, gl_texture_target_enum_type s_target, std::uint32_t s_slot) {
    return apply (s_tex.handle (), s_target, s_slot);
}

void ime::gl_context::texture_enable_slot (gl_texture_target_enum_type s_target, bool s_enable, std::uint32_t s_slot) {
    glActiveTexture (GL_TEXTURE0 + s_slot);
    (s_enable ? glEnable : glDisable) ((GLenum)s_target);
}


void ime::gl_context::draw (const ime::gl_render_pass& s_pass) {
    if (s_pass.program.use_count () < 1) {
        ime::log.warn (__FILE__ ": unable to draw anything without a valid program");
        return;
    }

    auto s_program = s_pass.program.lock ();
    
    auto s_slot = 0u;
    for (const auto& s_texture : s_pass.textures) {                     
        apply (s_texture.texture, s_texture.target, s_slot++);
    }

    for (const auto& s_shader_pass : s_program->passes ()) {
        apply (s_shader_pass);
        for (const auto& s_draw : s_pass.draw_calls) {
            apply (s_draw.vertex_array);
            draw_arrays (s_draw.mode, {
                    s_draw.offset, 
                    s_draw.offset 
                  + s_draw.length}, 
                s_draw.instances);
        }
    }
}
