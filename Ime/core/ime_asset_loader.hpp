#pragma once

#include <string>
#include <functional>

#include "ime_asset_base.hpp"
#include "ime_asset_locator.hpp"
#include "ime_asset_locator_file.hpp"
#include "ime_asset_locator_data.hpp"

namespace ime {

    struct asset_loader {
        typedef std::function<asset_loader& (const std::string&)> loader_locate_function;

        virtual ~asset_loader () = default;

        static asset_loader& locate (const std::string& s_mime_type);
        static loader_locate_function register_loader (const std::string& s_mine_type, loader_locate_function);
        static loader_locate_function unregister_loader (const std::string& s_mime_type);

        virtual asset_shared load (const std::string& s_key, const asset_locator&) const = 0;

    };


    inline auto plain_load (const std::string& s_key, const asset_locator& s_locator, const std::string& s_type = "") {
        return asset_loader::locate (s_type.empty () ? s_locator.type () : s_type).load (s_key, s_locator);
    }


    template <typename _Asset>
    inline auto plain_load_as_ (const std::string& s_key, const asset_locator& s_locator, const std::string& s_type = "") {
        auto s_asset = plain_load (s_key, s_locator, s_type);
        if (s_asset != nullptr) {
            return s_asset->as_<_Asset> ();
        }
        log.warn (__FILE__ ": unable able to load asset %s.", s_locator.location ());
        return std::shared_ptr<const _Asset> ();
    }
}
