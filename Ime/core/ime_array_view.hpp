#pragma once

#include <stdexcept>
#include <cstddef>
#include <cstdint>
#include <iterator>

namespace ime {

    namespace detail {
        template <typename T>
        struct array_view {
            typedef T value_type;
            typedef value_type& reference;
            typedef const value_type& const_reference;
            typedef value_type* pointer;
            typedef const value_type* const_pointer;
            typedef pointer iterator;
            typedef const_pointer const_iterator;
            typedef std::reverse_iterator<iterator> reverse_iterator;
            typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
            typedef decltype (iterator () - iterator ()) difference_type;
            typedef decltype (reverse_iterator () - reverse_iterator ()) reverse_difference_type;

            template <std::size_t _Number>
            array_view (value_type (&s_array) [_Number]) noexcept:
                array_view (std::data (s_array), std::size (s_array)) 
            {}

            array_view (pointer s_first, pointer s_last) noexcept :
                m_first (s_first),
                m_last (s_last) 
            {}

            array_view (pointer s_first, std::size_t s_count) noexcept :
                array_view (s_first, s_first + s_count) 
            {}

            array_view () noexcept :
                array_view (nullptr, nullptr) 
            {}

            std::size_t length () const noexcept { return m_last - m_first; }
            std::size_t size () const noexcept { return length (); }

            const_pointer data () const noexcept { return m_first; }
            const_iterator cbegin () const noexcept { return m_first; }
            const_iterator cend () const noexcept { return m_last; }
            const_iterator crbegin () const noexcept { return const_reverse_iterator (cend ()); }
            const_iterator crend () const noexcept { return const_reverse_iterator (cbegin ()); }

            const_reference operator [] (std::size_t i) const noexcept {
                return m_first [i];
            }
            const_reference at (std::size_t i) const {
                if (i < length ())
                    return m_first [i];
                throw std::out_of_range ("array_view<T>: index out of range.");
            }

        protected:
            iterator m_first;
            iterator m_last;
        };

        template <typename T>
        struct mutable_array_view:
            public array_view<T> {
            using array_view<T>::array_view;
            pointer  data ()   noexcept { return m_first; }
            iterator begin ()  noexcept { return m_first; }
            iterator end ()    noexcept { return m_last; }
            iterator rbegin () noexcept { return reverse_iterator (end ()); }
            iterator rend ()   noexcept { return reverse_iterator (begin ()); }

            reference operator [] (std::size_t i) noexcept {
                return m_first [i];
            }
            reference at (std::size_t i) noexcept {
                if (i < length ())
                    return m_first [i];
                throw std::out_of_range ("array_view<T>: index out of range.");
            }
        };
    }

    template <typename T>
    using const_array_view = detail::array_view<const T>;
    template <typename T>

    using mutable_array_view = detail::mutable_array_view<T>;

    template <typename T>
    struct array_view: mutable_array_view<T> {
        using mutable_array_view<T>::mutable_array_view;
    };

    template <typename T>
    struct array_view<const T>: const_array_view<T> {
        using const_array_view<T>::const_array_view;
    };

}