#pragma once

#include "ime_asset_loader.hpp"

namespace ime {
    struct asset_loader_image_png:
        public asset_loader {
        virtual asset_shared load (const std::string& s_key, const asset_locator&) const override;
        asset_loader_image_png ();
    };
}
