#pragma once

#include <memory>

namespace ime {

    struct gl_context;

    typedef std::weak_ptr<gl_context>   gl_context_weak;
    typedef std::shared_ptr<gl_context> gl_context_shared;

}
