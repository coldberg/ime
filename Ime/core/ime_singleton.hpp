#pragma once

#include <memory>

namespace ime {


    template <typename _Class>
    struct singleton {
        typedef std::shared_ptr<_Class> shared_type;
        typedef std::weak_ptr<_Class> weak_type;

        static shared_type lock ();

        singleton () = default;
        ~singleton () = default;
        singleton (const singleton&) = delete;
        singleton (singleton&&) = delete;
        singleton& operator = (const singleton&) = delete;
        singleton& operator = (singleton&&) = delete;
    };

    template <typename _Class>
    typename ime::singleton<_Class>::shared_type 
        ime::singleton<_Class>::lock () 
    {
        typedef _Class class_type;
        static typename singleton<class_type>::weak_type st_ptr;
        if (st_ptr.use_count () != 0) {
            return st_ptr.lock ();
        }
        try {
            auto s_ptr = std::make_shared<class_type> ();
            st_ptr = s_ptr;
            return s_ptr;
        }
        catch (const std::exception& s_ex) {
            ime::log.error (__FILE__ ": unexpected exception : %s\n", s_ex.what ());
        }
        return nullptr;
    }
}
