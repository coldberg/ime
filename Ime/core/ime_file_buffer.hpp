#pragma once

#include "ime_file_base.hpp"

namespace ime {

    struct immutable_file_buffer:
        public immutable_file_base 
    {

        ~immutable_file_buffer () override;

        std::size_t size () const override;

        std::uint64_t seek (std::uint64_t s_offset) override;
        std::uint64_t tell () const override;

        std::size_t read (void* s_buff, std::size_t s_length) override;

        const void* map_read_only (std::uint64_t s_offset, std::size_t s_size) override;

        void unmap () override;

        immutable_file_buffer (const void* s_buff, std::size_t s_size);

    protected:
        const void* underlying_data_buffer () const;
        void set_underlying_data_buffer (const void* s_buff, std::size_t);

    private:
        const std::uint8_t* m_buff;
        std::uint64_t m_size;
        std::uint64_t m_fptr;
    };


};
