#include "ime_example_render.hpp"
#include "ime_gl_context.hpp"
#include "ime_asset_loader.hpp"
#include "ime_gl_asset_texture.hpp"
#include "ime_gl_asset_shader.hpp"

#include <algorithm>

struct offset_item_type {
    ime::vec3 s_offset;
    ime::vec3 s_color;
    std::uint32_t s_index;
};

struct vertex_attr_type {
    ime::vec3 s_position;
    ime::vec2 s_texcoord;
};

ime::example_render::example_render (gl_context& s_context):
    m_context (s_context)
{
    static const char g_program_path [] = "materials/programs/basic.shader";
    static const char g_texture_path [] = "materials/textures/tiles.texture";

    m_program = ime::plain_load_as_<ime::gl_asset_shader> (g_program_path, ime::asset_locator_file (g_program_path), "glcore/shader");
    m_texture = ime::plain_load_as_<ime::gl_asset_texture> (g_texture_path, ime::asset_locator_file (g_texture_path), "glcore/texture");

    if (m_program == nullptr || m_texture == nullptr) {
        throw std::runtime_error (__FILE__ ": unable to load resources.");
    }

    m_mesh_buffer_per_vertex = m_context.buffer_create (std::vector<vertex_attr_type> {
        {ime::vec3{-50.0f, -50.0f, 0.0f}, ime::vec2{1.0f, 1.0f}},
        {ime::vec3{+50.0f, -50.0f, 0.0f}, ime::vec2{0.0f, 1.0f}},
        {ime::vec3{+50.0f, +50.0f, 0.0f}, ime::vec2{0.0f, 0.0f}},
        {ime::vec3{-50.0f, -50.0f, 0.0f}, ime::vec2{1.0f, 1.0f}},
        {ime::vec3{+50.0f, +50.0f, 0.0f}, ime::vec2{0.0f, 0.0f}},
        {ime::vec3{-50.0f, +50.0f, 0.0f}, ime::vec2{1.0f, 0.0f}}
    }, {ime::gl_buffer_access_map_write_bit});


    m_mesh_buffer_per_instance = s_context.buffer_create (sizeof (offset_item_type) * g_number_of_instances, {ime::gl_buffer_access_map_write_bit});

    m_mesh_array = s_context.vertex_array_create ();

    m_context.bind (m_mesh_array, m_mesh_buffer_per_vertex, 0, sizeof (vertex_attr_type), {
        {0, 3, ime::float32_data_type, offsetof (vertex_attr_type, s_position)},
        {1, 2, ime::float32_data_type, offsetof (vertex_attr_type, s_texcoord)}
    });

    m_context.bind (m_mesh_array, m_mesh_buffer_per_instance, 1, sizeof (offset_item_type), {
        {2, 3, ime::float32_data_type, offsetof (offset_item_type, s_offset)},
        {3, 3, ime::float32_data_type, offsetof (offset_item_type, s_color)},
        {4, 1, ime::uint32_data_type, offsetof (offset_item_type, s_index)},
    }, 1);

    m_render_pass.program = m_program;    
    m_render_pass.add_texture (m_texture->handle (), ime::gl_enum_texture2d_array);
    m_render_pass.add_draw_call (m_mesh_array, ime::gl_enum_triangles, 6u, g_number_of_instances, 0u);

}

void ime::example_render::update_objects (const ime::timer& s_timest, const ime::transform& s_transform) {
    const auto s_time = s_timest.now ();
    auto g_offset = m_context.buffer_map_as_array_of_<offset_item_type> (m_mesh_buffer_per_instance, {ime::gl_buffer_access_map_write_bit});

    for (auto i = 0; i < g_number_of_instances; ++i) {        
        const auto s_offset1 = i/(1.0f*g_number_of_instances) * ime::pi<float> () * 2.0f;        
        const auto fi = s_offset1 + 0.5f*s_time;
        const auto be = s_offset1 + 2.0f*s_time;
        const auto cfi = std::cos (fi);
        const auto sfi = std::sin (fi);
        const auto s_pi2over3 = ime::pi<float> ()*2.0f/3.0f;

        g_offset [i].s_offset = ime::vec3 (sfi, 0.0f, cfi)*500.0f;
        g_offset [i].s_color = (1.0f + ime::vec3 (sin (be - s_pi2over3), sin (be), sin (be + s_pi2over3)))*0.5f;
        g_offset [i].s_index = i;
    }

    auto s_projection_view = s_transform.m_projection_view;

    std::sort (
        std::begin (g_offset),
        std::end (g_offset),
        [&s_projection_view] (const auto& a, const auto& b) {
            return (s_projection_view * ime::vec4 (a.s_offset, 1)).z
                 > (s_projection_view * ime::vec4 (b.s_offset, 1)).z;
        }
    );

    m_context.buffer_unmap (m_mesh_buffer_per_instance);

}

void ime::example_render::render_frame (const ime::timer& s_time, const ime::transform& s_transform, const ime::frustrum& s_frustrum) {
    update_objects (s_time, s_transform);
    m_context.uniform (*m_program, "g_transform", s_transform.m_projection_view);
    m_context.uniform (*m_program, "g_texture0", 0);
    m_context.draw (m_render_pass);
}
