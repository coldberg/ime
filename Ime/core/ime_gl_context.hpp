#pragma once

#include <memory>
#include <vector>
#include <array>
#include <type_traits>

#include "ime_math.hpp"
#include "ime_small_set.hpp"
#include "ime_singleton.hpp"
#include "ime_array_view.hpp"
#include "ime_asset_type_image.hpp"
#include "ime_gl_context_fwd.hpp"
#include "ime_gl_handle_fwd.hpp"
#include "ime_gl_shader_props.hpp"
#include "ime_gl_shader_pass_fwd.hpp"
#include "ime_gl_asset_shader_fwd.hpp"
#include "ime_gl_asset_texture_fwd.hpp"
#include "ime_gl_render_pass.hpp"
#include "ime_gl_enum.hpp"
#include "ime_mpl.hpp"


namespace ime {

    struct gl_context: 
        public singleton<gl_context> 
    {
        
        template<typename, typename> 
        friend struct gl_handle_non_copyable;

        gl_context ();        
       ~gl_context ();

    //* VIEWPORT MANAGEMENT

        auto window (const ime::ivec2& s_size); 
        auto window () const -> std::pair<ime::ivec2, ime::ivec2>;
        auto window (const ime::ivec2& s_origin, const ime::ivec2& s_size);
        auto viewport () const -> std::pair<ime::ivec2, ime::ivec2>;
        void viewport (const ime::ivec2& s_origin, const ime::ivec2& s_size);
        auto scissor () const -> std::pair<ime::ivec2, ime::ivec2>;
        void scissor (const ime::ivec2& s_origin, const ime::ivec2& s_size);
        void scissor_enabled (bool);
        bool scissor_enabled () const;

    //* SHADER MANAGEMENT

        gl_program_handle program_create () const;
        gl_shader_handle shader_create  (gl_stage_enum_type s_type) const;

        bool shader_compile (const gl_shader_handle& s_obj, const std::string& s_text, std::vector<std::string>& s_log) const;
        gl_shader_handle shader_compile (gl_stage_enum_type s_type, const std::string& s_text, std::vector<std::string>& s_log) const;

        bool program_link (const gl_program_handle& s_prg, const std::vector<gl_shader_handle>& s_objs, std::vector<std::string>& s_log) const;
        gl_program_handle program_link (const std::vector<gl_shader_handle>& s_objs, std::vector<std::string>& s_log) const;

        void apply (const gl_program_handle&) const;
        void apply (const gl_shader_props&) const;
        void apply (const gl_shader_pass&) const;

    //* UNIFORM SETTING

        int uniform_location (const gl_program_handle& s_prg, const char* s_name) const;
        int attribute_location (const gl_program_handle& s_prg, const char * s_name) const;
    
    protected:
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const std::int32_t*  s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const std::uint32_t* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const float* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const double* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::vec4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::quat* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::ivec4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::iquat* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uvec4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::uquat* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec2 * s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec3 * s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dvec4 * s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dquat * s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat2x4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat3x4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::mat4x4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat2x4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat3x4* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x2* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x3* s_data) const;
        void uniform_array_internal (const gl_program_handle& s_prg, int s_loc, std::size_t s_count, const ime::dmat4x4* s_data) const;

        template <typename _Value> 
        void uniform_array_internal (const gl_program_handle& s_prg, const char* s_loc, std::size_t s_count, const _Value* s_data) const;

        template <typename _Value>
        void uniform_array_internal (const gl_program_handle& s_prg, const std::string& s_loc, std::size_t s_count, const _Value* s_data) const;
    public:

        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_program_handle& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const;
       
        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_program_handle& s_prg, const _Name& s_loc, const _Value& s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int> = 0>
        void uniform (const gl_program_handle& s_prg, const _Name& s_loc, const _Value& s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_shader_pass& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_shader_pass& s_prg, const _Name& s_loc, const _Value& s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int> = 0>
        void uniform (const gl_shader_pass& s_prg, const _Name& s_loc, const _Value& s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_asset_shader& s_prg, const _Name& s_loc, std::size_t s_count, const _Value* s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_arithmetic<_Value>::value, int> = 0>
        void uniform (const gl_asset_shader& s_prg, const _Name& s_loc, const _Value& s_data) const;

        template <typename _Name, typename _Value, std::enable_if_t<is_container<_Value>::value, int> = 0>
        void uniform (const gl_asset_shader& s_prg, const _Name& s_loc, const _Value& s_data) const;


    //* BUFFER MANAGEMENT

        gl_buffer_handle buffer_create (const void* s_data, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;
        gl_buffer_handle buffer_create (std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const { return buffer_create (nullptr, s_size, s_access); }
        template <typename _Container, typename _Value = decltype (*std::data (std::declval<_Container> ()))>
        gl_buffer_handle buffer_create (const _Container& s_view, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;

        std::size_t buffer_size (const gl_buffer_handle& s_buffer) const;

        void buffer_update (gl_buffer_handle& s_buffer, const void* s_data, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access = {ime::gl_buffer_access_map_write_bit}) const;
        template <typename _Container, typename _Value = decltype (*std::data (std::declval<_Container> ()))>
        void buffer_update (gl_buffer_handle& s_buffer, const _Container& s_view, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;

        void buffer_flush (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size) const;
        void buffer_flush (const gl_buffer_handle& s_buffer, std::size_t s_size) const;
        void buffer_flush (const gl_buffer_handle& s_buffer) const;

        void* buffer_map (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;
        void* buffer_map (const gl_buffer_handle& s_buffer, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;
        void* buffer_map (const gl_buffer_handle& s_buffer, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;

        template <typename T>
        ime::array_view<T> buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, std::size_t s_offset, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access, bool s_align) const;
        template <typename T>
        ime::array_view<T> buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, std::size_t s_size, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;
        template <typename T>
        ime::array_view<T> buffer_map_as_array_of_ (const gl_buffer_handle& s_buffer, small_set<gl_buffer_access_flags_enum_type, std::uint32_t> s_access) const;

        void buffer_unmap (const gl_buffer_handle& s_buffer) const;

    //* VERTEX ARRAY MANAGEMENT

                        // index, count components, data type, relative offset
        typedef std::tuple<std::uint32_t, std::uint32_t, gl_data_type_enum_type, std::size_t> buffer_binding_descriptor_type;

        gl_vertex_array_handle vertex_array_create ();
        void bind (gl_vertex_array_handle& s_vao, const gl_buffer_handle&, std::uint32_t s_ibind, std::size_t s_stride, const std::vector<buffer_binding_descriptor_type>&, std::uint32_t s_per_instance = 0) const;
        void apply (const gl_vertex_array_handle&);

    //* TEXTURE MANAGEMENT

        gl_texture_handle texture_create (gl_texture_target_enum_type s_target, gl_texel_format_enum_type s_format, const uvec3& s_extent, std::uint32_t s_num_levels) const;
        void texture_update (const gl_texture_handle& s_texture, gl_texture_target_enum_type s_target, const uvec3& s_offset, const uvec3& s_extent, const void* s_data, const image_format_descriptor& s_format, std::uint32_t s_mipmap_level) const;
        void texture_build_mipmaps (const gl_texture_handle& s_texture) const;

        void texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, float s_value) const;
        void texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, int s_value) const;
        void texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::vec4& s_value) const;
        void texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::ivec4& s_value, float s_normalize = false) const;
        void texture_parameter (const gl_texture_handle& s_handle, gl_texture_parameter_enum_type s_param, const ime::uvec4& s_value) const;

        ime::ivec3 texture_extent (const gl_texture_handle& s_handle, std::uint32_t s_level) const;

        void apply (const gl_texture_handle& s_tex, gl_texture_target_enum_type s_target = gl_enum_texture2d, std::uint32_t s_slot = 0u);
        void apply (const gl_asset_texture& s_tex, gl_texture_target_enum_type s_target = gl_enum_texture2d, std::uint32_t s_slot = 0u);
        void texture_enable_slot (gl_texture_target_enum_type s_target = gl_enum_texture2d, bool s_enable = true, std::uint32_t s_slot = 0u);        

    //* DRAW CALLS
        
        void clear (small_set<ime::gl_clear_flags_enum_type> s_flags, const ime::vec4& s_color = ime::vec4(1.0f), float s_depth = 1.0f, int s_stencil = 0);

        void draw_arrays (const gl_shader_pass& s_pass0, const gl_vertex_array_handle& s_handle, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range);
        void draw_arrays (const gl_asset_shader& s_pass, const gl_vertex_array_handle& s_handle, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range);
        void draw_arrays (const gl_shader_pass& s_pass0, const gl_vertex_array_handle& s_handle, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances);
        void draw_arrays (const gl_asset_shader& s_pass, const gl_vertex_array_handle& s_handle, gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances);

        void draw_arrays (gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range);
        void draw_arrays (gl_draw_mode_enum_type s_mode, const std::pair<std::uint32_t, std::uint32_t>& s_range, std::uint32_t s_instances);

        void draw (const gl_render_pass&);

        void present ();

        //* RELEASE RESOURCES
    protected:
        void release (gl_shader_handle&) const;
        void release (gl_program_handle&) const;
        void release (gl_buffer_handle&) const;
        void release (gl_texture_handle&) const;
        void release (gl_vertex_array_handle&) const;
    };

}

#include "ime_gl_context_inl.hpp"