#pragma once

#include <chrono>
#include <SDL.h>

namespace ime {

    template <typename _Precision = float>
    struct basic_timer {
        typedef _Precision precision_type;
        //typedef _Resolution resolution_type;
        
        static precision_type now () {
            /*
            const auto t = std::chrono::high_resolution_clock::now ().time_since_epoch ();
            static const auto ratio = precision_type (resolution_type::period::num)
                                    / precision_type (resolution_type::period::den);
            return std::chrono::duration_cast<resolution_type> (t).count () * ratio;
            */
            return SDL_GetTicks ()* precision_type(1e-3);
        };

        const precision_type& starting_point () const {
            return m_timestamp0;
        }

        const precision_type& ending_point () const {
            return m_timestamp1;
        }

        void advance () {
            m_timestamp0 = m_timestamp1;
            m_timestamp1 = now ();
        }

        precision_type delta_time () const {
            return m_timestamp1 - m_timestamp0;
        }

    private:
        precision_type m_timestamp0 = now ();
        precision_type m_timestamp1 = now ();
    };

    typedef basic_timer</*std::chrono::milliseconds, */float> timer;
}