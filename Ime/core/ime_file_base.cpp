#include "ime_file_base.hpp"

ime::immutable_file_base::immutable_file_base ():
    m_last_error (io_status_success)
{}

void ime::immutable_file_base::last_error (io_status_type s_error) {
    m_last_error = s_error;
}

ime::io_status_type ime::immutable_file_base::last_error () const {
    return m_last_error;
}

ime::io_status_type ime::immutable_file_base::last_error_clear () {
    const auto s_tmp = last_error ();
    last_error (io_status_success);
    return s_tmp;
}

bool ime::immutable_file_base::is_good () const {
    return last_error () == io_status_success;
}
