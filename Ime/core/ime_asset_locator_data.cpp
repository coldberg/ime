#include "ime_asset_locator_data.hpp"
#include "ime_asset_loader.hpp"
#include "ime_asset_cache.hpp"
#include "ime_file_buffer.hpp"
#include "ime_utils.hpp"

const char* ime::asset_locator_data::type () const { return m_mime_type.c_str (); }

const char* ime::asset_locator_data::location () const {
    return "internal";
}

ime::asset_locator_data::asset_locator_data (
    const std::string& s_fake_path, 
    const std::string& s_mime_type,
    const void * s_data_addr, 
    std::size_t s_data_size, 
    std::uint32_t s_flags)
:   m_mime_type (s_mime_type),
    m_data_size (s_data_size),
    m_data_addr (s_flags & copy_data        
                 ? duplicate_data (s_data_addr, s_data_size)
                 : s_data_addr),
    m_file_flag (s_flags)
{}

ime::asset_locator_data::~asset_locator_data () {
    if ((m_file_flag & copy_data) && m_data_addr != nullptr) {
        free ((void*)m_data_addr);
    }
}

auto ime::asset_locator_data::access () const -> 
    std::shared_ptr<ime::immutable_file_base> 
{
    return std::make_shared<ime::immutable_file_buffer> (data (), size ());
}

const void* ime::asset_locator_data::data () const {
    return m_data_addr;
}

std::size_t ime::asset_locator_data::size () const {
    return m_data_size;
}

const void* ime::asset_locator_data::duplicate_data (const void* s_data_addr, 
                                                     std::size_t s_data_size) 
{
    auto s_new_addr = malloc (s_data_size);
    memcpy (s_new_addr, s_data_addr, s_data_size);
    return s_new_addr;
}
