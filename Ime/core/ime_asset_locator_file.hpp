#pragma once

#include "ime_asset_locator.hpp"
#include "ime_mime.hpp"

namespace ime {

    struct asset_locator_file: 
        public asset_locator_t<asset_locator_file>
    {
        asset_locator_file (const std::string& s_file_path, const std::string& s_mime_type);
        asset_locator_file (const std::string& s_file_path);

        const char* type () const override;
        const char* location () const override;
        virtual const char* file_path () const;

        auto access () const -> std::shared_ptr<ime::immutable_file_base> override;

    private:
        std::string m_file_path;
        std::string m_mime_type;
    };

}