#include "ime_gl_loader_shader.hpp"
#include "ime_asset_type_ptree.hpp"
#include "ime_asset_type_text.hpp"
#include "ime_asset_cache.hpp"
#include "ime_gl_context.hpp"
#include "ime_gl_shader_pass.hpp"
#include "ime_gl_asset_shader.hpp"
#include "ime_gl_handle.hpp"
#include "ime_utils.hpp"

#include <iostream>
#include <unordered_map>

ime::gl_core_shader_loader::~gl_core_shader_loader () {}

ime::asset_shared ime::gl_core_shader_loader::load (const std::string& s_key, const asset_locator& s_locator) const {
    auto s_ptree_asset = ime::plain_load_as_<ime::asset_ptree> (s_key, s_locator, "text/ptree");
    if (s_ptree_asset == nullptr) {
        log.warn (__FILE__ ": unable to load shader properties.");
        return nullptr;
    }    

    auto s_root = s_ptree_asset->ptree ().get_child_optional ("program");
    if (s_root.is_initialized ()) {
        return load (s_key, *s_root);
    }
    ime::log.warn (__FILE__ ": unable to parse \"%s\".", s_locator.location ());
    return nullptr;    
}

ime::asset_shared ime::gl_core_shader_loader::load (const std::string& s_key, const boost::property_tree::ptree& s_root) const {
    using namespace boost::property_tree;
    using namespace boost;    

    typedef std::function<void (ime::gl_shader_props& s_params, const boost::property_tree::ptree& s_keyval)> key_read_func_type;
    static const std::unordered_map<std::string, key_read_func_type> st_property_callback = {
        {"depth", [] (auto& s_params, const auto& s_child) {}},
        {"depth.write", [] (auto& s_params, const auto& s_child) {
            s_params.depth.write = s_child.get_value<bool> ();
        }},
        {"depth.test", [] (auto& s_params, const auto& s_child) {
            s_params.depth.test = s_child.get_value<bool> ();
        }},
        {"blend", [] (auto& s_params, const auto& s_child) {}},
        {"blend.enabled", [] (auto& s_params, const auto& s_child) {
            s_params.blend.enabled = s_child.get_value<bool> ();
        }},
        {"blend.src_factor", [] (auto& s_params, const auto& s_child) {           
            s_params.blend.src_factor = ime::gl_enum_cast<ime::gl_blend_func_enum_type> (s_child.get_value<std::string> ());
        }},
        {"blend.dst_factor", [] (auto& s_params, const auto& s_child) {        
            s_params.blend.dst_factor = ime::gl_enum_cast<ime::gl_blend_func_enum_type> (s_child.get_value<std::string> ());
        }},
        {"blend.equation", [] (auto& s_params, const auto& s_child) {
            s_params.blend.equation = ime::gl_enum_cast<ime::gl_blend_equation_enum_type> (s_child.get_value<std::string> ());
        }}
    };

    auto s_core = ime::gl_context::lock ();
    if (s_core == nullptr) {
        log.warn (__FILE__ ": unable to load shader, gl_core not initialized.");
        return nullptr;
    }

    const auto& s_attr = s_root.get_child_optional ("name");
    auto s_shader_name = std::string ("untitled");
    if (s_attr.is_initialized ()) {
        s_shader_name = s_attr->get_value<std::string> ();
    }
    
    static const auto s_stage_data = 
        std::vector<std::pair<gl_stage_enum_type, std::string>> 
    {
        {gl_enum_vert_stage, "vert"},
        {gl_enum_tesc_stage, "tesc"},
        {gl_enum_tese_stage, "tese"},
        {gl_enum_geom_stage, "geom"},
        {gl_enum_frag_stage, "frag"},
        {gl_enum_comp_stage, "comp"},
    };

    std::vector<ime::gl_shader_pass> s_passes;

    auto s_global_params = gl_shader_props::default_props ();
    process_properties (st_property_callback, s_root, s_global_params, "properties");

    for_each (s_root.equal_range ("pass"), [&] (const auto& s_pass) {
        int s_stage_count [6] = {0, 0, 0, 0, 0, 0};

        std::vector<gl_shader_handle> s_shaders;

        for (const auto& s_stage : s_stage_data) {
            for_each (s_pass.second.equal_range (s_stage.second), [&] (const auto& s_sref) {
                auto s_path = s_sref.second.get_value<std::string> ();
                auto s_text = ime::asset_cache ().fetch_as_<ime::asset_text> (s_path, "text/plain");
                if (s_text == nullptr) {
                    ime::log.warn (__FILE__ ": unable to fetch %s.", s_path.c_str ());
                    return;                        
                }
                
                std::vector<std::string> s_log;
                auto s_object = s_core->shader_compile (s_stage.first, s_text->string (), s_log);

                if (s_log.size () > 0) {
                    ime::log.debug (__FILE__ ": `%s` compile error log...", s_path.c_str ());
                    for (const auto& s_item : s_log) {
                        ime::log.debug ("%s", s_item.c_str ());
                    }
                }
                if (s_object.value () == 0u) {
                    ime::log.warn (__FILE__ ": unable to compiler shader.");
                    return;
                }                    
                s_shaders.push_back (std::move (s_object));
                ++s_stage_count [s_stage.first];
            });
        }            

        if((s_stage_count [gl_enum_frag_stage] != 0
         && s_stage_count [gl_enum_vert_stage] != 0)
         || s_stage_count [gl_enum_comp_stage] != 0)
        {
            std::vector<std::string> s_log;
            auto s_program = s_core->program_link (s_shaders, s_log);
            
            if (s_log.size () > 0) {
                ime::log.debug (__FILE__ ": `%s` link error log...", s_key.c_str ());
                for (const auto& s_item : s_log) {
                    ime::log.debug ("%s", s_item.c_str ());
                }
            }

            if (s_program.value () == 0u) {
                ime::log.warn (__FILE__ ": unable to link program %s.", s_key.c_str ());
                return;
            }

            auto s_local_params = s_global_params;
            process_properties (st_property_callback, s_pass.second, s_local_params, "properties");
            s_passes.push_back (ime::gl_shader_pass (std::move (s_program), s_local_params));
            return;
        }
        ime::log.warn (__FILE__ ": invalid pass configuration, must atleast contain a vertex and a fragment stage.");                
    });

    if (s_passes.size () < 1) {
        ime::log.warn (__FILE__ ": unable to load shader, no valid passes defined.");
        return nullptr;
    }
    
    return ime::gl_asset_shader::new_shared (s_shader_name, std::move (s_passes), s_global_params);
}

IME_BEFORE_MAIN ([] () {
    static ime::gl_core_shader_loader g_loader;
    ime::asset_loader::register_loader ("glcore/shader", 
    [] (const std::string& s_type) -> ime::asset_loader& {
        return g_loader;
    });
});
