#include "ime_camera_perspective.hpp"

ime::camera_perspective::camera_perspective (const ime::mat3& s_frame, float s_aspect, float s_fov, float s_near, float s_far):
    m_frame (s_frame),
    m_aspect (s_aspect),
    m_fov (s_fov),
    m_near (s_near),
    m_far (s_far)
{
    dirty_projection ();
    dirty_view ();
}

const ime::mat3& ime::camera_perspective::frame () const { return m_frame; }
const float& ime::camera_perspective::aspect () const { return m_aspect; }
const float& ime::camera_perspective::fov () const { return m_fov; }
const float& ime::camera_perspective::near () const { return m_near; }

void ime::camera_perspective::frame (const ime::mat3 & s_value) {
    m_frame = s_value;
    dirty_view ();
}

void ime::camera_perspective::aspect (const float & s_value) {
    m_aspect = s_value;
    dirty_projection ();
}

void ime::camera_perspective::fov (const float & s_value) {
    m_fov = s_value;
    dirty_projection ();
}

void ime::camera_perspective::near (const float & s_value) {
    m_near = s_value;
    dirty_projection ();
}

ime::vec3 ime::camera_perspective::upward () const {
    return ime::normalize (m_frame [2]);
}

ime::vec3 ime::camera_perspective::forward () const {
    return ime::normalize (m_frame [1]);
}

ime::vec3 ime::camera_perspective::rightward () const {
    return ime::cross (upward (), forward ());
   
}

void ime::camera_perspective::move (const ime::vec3& m_offset) {
    m_frame [0] += m_offset;
    dirty_view ();
}

void ime::camera_perspective::rotate (const ime::quat& m_offset) {
    m_frame [1] = m_offset * m_frame [1] * ime::conjugate (m_offset);
    m_frame [2] = m_offset * m_frame [2] * ime::conjugate (m_offset);
    dirty_view ();
}

void ime::camera_perspective::far (const float & s_value) {
    m_far = s_value;
    dirty_projection ();
}

void ime::camera_perspective::dirty_projection () {
    cache_projection (ime::perspective (m_fov, m_aspect, m_near, m_far));
}

void ime::camera_perspective::dirty_view () {
    cache_view (ime::lookAt (m_frame [0], m_frame [0] + m_frame [1], m_frame [2]));
}
